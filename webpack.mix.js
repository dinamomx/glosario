/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable class-methods-use-this */
const mix = require('laravel-mix')
const presetEnv = require('postcss-preset-env')
const cssnano = require('cssnano')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const isProd = process.env.NODE_ENV === 'production'

mix
  .js('resources/assets/js/app.js', 'public/js')
  .js('resources/assets/js/public.js', 'public/js')
  .js('resources/assets/js/dashboard.js', 'public/js')
  .extract([
    'vue',
    '@fortawesome/vue-fontawesome',
    '@fortawesome/fontawesome-svg-core',
    // './resources/assets/js/lib/buefy/src/index',
  ])
  .sass('resources/assets/sass/app.scss', 'public/css')
  .options({
    processCssUrls: false,
  })
  .sass('resources/assets/sass/global.scss', 'public/css')
  .sass('resources/assets/sass/layouts/dashboard.scss', 'public/css/layouts')
  .sass('resources/assets/sass/layouts/default.scss', 'public/css/layouts')
  .sass('resources/assets/sass/layouts/auth.scss', 'public/css/layouts')
  .sass('resources/assets/sass/transitions.scss', 'public/css')
  .sass('resources/assets/sass/layouts/pdf.scss', 'public/css')
  .options({
    processCssUrls: false,
    // extractVueStyles: true,
    postCss: {
      plugins: [
        presetEnv({
          // NOTE: https://github.com/postcss/autoprefixer#does-autoprefixer-polyfill-grid-layout-for-ie
          autoprefixer: {
            grid: true,
          },
        }),
        cssnano({ preset: 'default' }),
      ],
    },
  })
  .sourceMaps()
  // .autoload(({
  //   quill: ['window.Quill'],
  // }))

if (isProd) {
  mix
    .webpackConfig(() => ({
      plugins: [
        new LodashModuleReplacementPlugin({
          deburring: true,
          unicode: true,
        }),
        // new BundleAnalyzerPlugin(),
      ],
    }))
    .version()
} else {
  mix.browserSync('glosario.test')
}
