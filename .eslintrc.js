module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 6,
    sourceType: 'module'
  },
  extends: [
    // "plugin:prettier/recommended",
    'airbnb-base',
    'plugin:vue/recommended'
  ],
  // required to lint *.vue files
  plugins: [
    // "prettier"
    // 'html'
  ],
  // add your custom rules here
  rules: {
    semi: ["error", "never"],
    "vue/html-closing-bracket-newline": 0,
    // "prettier/prettier": ["error", {
    //   "printWidth": 80,
    //   "singleQuote": true,
    //   "bracketSpacing": true,
    //   "jsxBracketSameLine": true,
    //   "trailingComma": "all",
    //   "arrowParens": "always",
    //   "semi": false
    // }],
    'no-console': 1,
    'no-debugger': 1,
    // 'arrow-parens': ['error', 'allways'],
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'always'
    }],
    'no-plusplus': ["error", { "allowForLoopAfterthoughts": true }],
    'no-param-reassign': ["error", { "props": false }],
    // 'no-use-before-define': 0,
  }
}
