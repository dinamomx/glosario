<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => "Glosario para en linea ", // set false to total remove
            'description'  => 'Consulta interactiva', // set false to total remove
            'separator'    => ' - ',
            'keywords'     => ['igualdad', 'inmujeres', 'feminismo'],
            'canonical'    => null, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'        => "Glosario para la igualdad ", // set false to total remove
            'description'  => 'Consulta en linea', // set false to total remove
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => 'website',
            'site_name'   => 'Inmujeres',
            'images'      => [env('APP_URL', 'http://localhost')."/images/logo_fb.png"],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          'card'        => 'summary',
          'site'        => '@inmujeres',
        ],
    ],
];
