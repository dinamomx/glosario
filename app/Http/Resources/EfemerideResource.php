<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class EfemerideResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'object'         => 'Efemeride',
            'id'             => $this->id,
            'date'           => $this->date,
            'name'           => $this->name,
            'slug'           => $this->slug,
            'description'    => $this->description,
            'elements'       => $this->elements,
            'user_id'        => $this->user_id,
            'thumbnail_path' => $this->thumbnail_path,
            'thumbnail_url'  => $this->thumbnail_url,
            'thumbnail_desc' => $this->thumbnail_desc,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
            'api_permalink'  => route('api.efemerides.show', $this),
            'permalink'      => route('efemerides.show', $this),
            'edit'           => $this->when(Auth::user(), function () {
                return route('admin.efemerides.edit', $this);
            }),
        ];
    }
}
