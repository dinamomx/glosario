<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class TerminoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'object'         => 'Termnio',
            'id'             => $this->id,
            'name'           => $this->name,
            'description'    => $this->description,
            'elements'       => $this->elements,
            'user_id'        => $this->user_id,
            'thumbnail_path' => $this->thumbnail_path,
            'thumbnail_url'  => $this->thumbnail_url,
            'thumbnail_desc' => $this->thumbnail_desc,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
            'permalink'      => route('terminos.show', $this),
            'api_permalink'  => route('api.terminos.show', $this),
            'edit'           => $this->when(Auth::user(), function () {
                return route('admin.terminos.edit', $this);
            }),
        ];
    }
}
