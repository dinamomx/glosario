<?php

namespace App\Http\Controllers;

use App\Termino;
use Illuminate\Http\Request;
use SEO;

/**
 * Termino controler
 * Controlador del termino para vistas publicas
 *
 * @category  Controllers
 * @package   GlosarioInteractivo
 * @author    César Valadez <cesar@wdinamo.com>
 * @copyright 2018 Dinamo https://dinamo.mx
 * @license   https://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @link      https://laravel.com/docs/5.4/controllers
 */
class TerminoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        SEO::setTitle('Términos');
        SEO::setDescription('Terminos de la igualdad');
        SEO::opengraph()->addProperty('type', 'articles');
        $terminos = Termino::paginate(40);
        return view('terminos.index', ['terminos' => $terminos]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function populares()
    {
        SEO::setTitle('Términos populares');
        SEO::setDescription('Terminos de la igualdad');
        SEO::opengraph()->addProperty('type', 'articles');
        $terminos = Termino::ranked('hits')->paginate(40);
        return view('terminos.index', ['terminos' => $terminos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Termino  $termino
     * @return \Illuminate\Http\Response
     */
    public function show(Termino $termino)
    {
        $termino->load('relatedTo', 'relatedBy');
        $next = Termino::where('name', '>', $termino->name)
            ->orderBy('name')
            ->select('id','name', 'slug')
            ->first();
        $prev = Termino::where('name', '<', $termino->name)
            ->orderBy('name', 'desc')
            ->select('id','name', 'slug')
            ->first();
        SEO::setTitle($termino->name);
        SEO::setDescription(str_limit(strip_tags($termino->description), 200));
        SEO::opengraph()->addProperty('type', 'articles');
        return view('terminos.show', ['termino' => $termino, 'next' => $next, 'prev' => $prev]);
    }

    /**
     * Registra un hit
     *
     * Se espera únicamente que sea llamado por ajax
     *
     * @param \App\Termino $termino
     * @return \Illuminate\Http\Response
     **/
    public function hit(Termino $termino, Request $request)
    {
        // Evitando bots
        $input =  $request->all();
        if ($request->ajax() && $request->has(['a', 'b', 'c'])) {
            // Usando técinca de trios para detectar bots
            // https://www.kalzumeus.com/2010/06/07/detecting-bots-in-javascrip/
            $numberA = (int) $request->a;
            $numberB = (int) $request->b;
            $numberC = (int) $request->c;
            $result = $numberA + $numberB;
            if ($result === $numberC) {
                $count = $termino->hit();
                return response()->json(['ok', $count], 202);
            }
        }
        return response()->json(['message' => 'missing a, b and c', 'data' => $input], 401);
    }
}
