<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Efemeride;
use App\Termino;
use App;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    const NUMBER_OF_EFEMERIDES = 2;

    public function index()
    {
        //  Usar comparacón de mes y dia
        $now = Carbon::now(new \DateTimeZone('America/Mexico_City'));
        $day = $now->day;
        $month = $now->month;
        // Busca las siguientes basandose en el día del año (cualquier año)
        $today_efemeride = Efemeride::whereRaw("({$day} = DAYOFMONTH(date) and {$month} = MONTH(DATE))")
            ->get();
        $efemerides_closer = Efemeride::whereRaw("({$day} < DAYOFMONTH(date) and {$month} <= MONTH(DATE))")
            ->limit(self::NUMBER_OF_EFEMERIDES)
            ->get();
        if ($efemerides_closer->count() < self::NUMBER_OF_EFEMERIDES) {
            $missing_femerides = self::NUMBER_OF_EFEMERIDES - $efemerides_closer->count();
            $efemerides_closer = $efemerides_closer->merge(Efemeride::limit($missing_femerides)->get());
        }

        return view('welcome',[
            'top_efemerides'  => $efemerides_closer,
            'today_efemeride' => $today_efemeride,
        ]);
    }
    /**
     * Search controler
     * Se usa para la barra de busqueda global
     *
     * @param Request $request
     * @return void
     */
    public function search(Request $request)
    {
        if ($request->has('search')) {
            $terminos = Termino::search($request->search)->paginate(15);
            $efemerides = Efemeride::search($request->search)->paginate(15);
            $request->flash();
        } else {
            $terminos = Termino::paginate(15);
            $efemerides = Efemeride::paginate(15);
        }
        return view('search', ['terminos' => $terminos, 'efemerides' => $efemerides]);
    }

}
