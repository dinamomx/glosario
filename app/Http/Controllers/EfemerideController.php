<?php

namespace App\Http\Controllers;

use App\Efemeride;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateEfemerideRequest;
use App\Http\Requests\CreateEfemerideRequest;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use SEO;

class EfemerideController extends Controller
{
    const NUMBER_OF_RELATED = 4;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        SEO::setTitle('Efemerides');
        SEO::setDescription('Efemerides de la igualdad');
        SEO::opengraph()->addProperty('type', 'articles');
        $efemerides = Efemeride::paginate(10);
        return view('efemerides.index', ['efemerides' => $efemerides]);
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function populares()
    {
        SEO::setTitle('Efemerides populares');
        SEO::setDescription('Las efemerides más populares');
        SEO::opengraph()->addProperty('type', 'articles');
        $efemerides = Efemeride::ranked('hits')->paginate(10);
        return view('efemerides.index', ['efemerides' => $efemerides]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Efemeride  $efemeride
     * @return \Illuminate\Http\Response
     */
    public function show(Efemeride $efemeride)
    {
        SEO::setTitle($efemeride->name);
        SEO::setDescription(str_limit(strip_tags($efemeride->description), 200));
        SEO::opengraph()->addProperty('type', 'articles');
        // Busca las siguientes basandose en el día del año (cualquier año)
        $related = Efemeride::whereRaw("{$efemeride->date->month} = MONTH(date)")
            ->whereNotIn('id', [$efemeride->id])
            ->limit(self::NUMBER_OF_RELATED)
            ->get();

        return view('efemerides.show', ['efemeride' => $efemeride, 'related' => $related]);
    }


    /**
     * Registra un hit
     *
     * Se espera únicamente que sea llamado por ajax
     *
     * @param \App\Efemeride $termino
     * @return \Illuminate\Http\Response
     **/
    public function hit(Efemeride $efemeride, Request $request)
    {
        // Evitando bots
        if ($request->ajax() && $request->has(['a', 'b', 'c'])) {
            // Usando técinca de trios para detectar bots
            // https://www.kalzumeus.com/2010/06/07/detecting-bots-in-javascrip/
            $numberA = (int) $request->a;
            $numberB = (int) $request->b;
            $numberC = (int) $request->c;
            $result = $numberA + $numberB;
            if ($result === $numberC) {
                $count = $efemeride->hit();
                return response()->json(['ok', $count], 202);
            }
        }
        return response()->json(['missing'], 404);
    }
}
