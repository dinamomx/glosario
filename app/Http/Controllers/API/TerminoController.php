<?php

namespace App\Http\Controllers\API;

use App\Termino;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TerminoResource;
use App\Http\Resources\TerminoCollection;

class TerminoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return TerminoResource::collection(Termino::paginate(15));
        return new TerminoCollection(Termino::paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Termino  $termino
     * @return \Illuminate\Http\Response
     */
    public function show(Termino $termino)
    {
        return new TerminoResource($termino);
    }

}
