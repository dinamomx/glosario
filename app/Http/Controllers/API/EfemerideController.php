<?php

namespace App\Http\Controllers\API;

use App\Efemeride;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EfemerideResource;
use App\Http\Resources\EfemerideCollection;

class EfemerideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new EfemerideCollection(Efemeride::paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Efemeride  $efemeride
     * @return \Illuminate\Http\Response
     */
    public function show(Efemeride $efemeride)
    {
        return new EfemerideResource($efemeride);
    }

}
