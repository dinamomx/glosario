<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Efemeride;
use Illuminate\Support\Facades\Storage;
use App\Termino;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $efemerides_count = Efemeride::count();
        $terminos_count = Termino::count();
        return view('admin.index', [
            'efemerides_count' => $efemerides_count,
            'terminos_count' => $terminos_count
        ]);
    }

    /**
     * Sube una imagen, se usa en la creación de elementos
     *
     * @param Request $request
     * @return Response
     **/
    public function storeFile(Request $request)
    {
        $file = $request->file('file');
        $folder = $request->folder ?? 'content';
        if ($file) {
            try {
                $path = $request->file('file')->store($folder, 'public');
                return response()->json([
                    'path' => $path,
                    'url' => asset(Storage::url($path)),
                ], 201);
            } catch (Exception $e) {
                $request->session()->push('errors', 'No se pudo guardar la imagen');
                return response()->json(['error' => 'No se pudo guardar la imagen'], 400 );
            }
        } else {
            $request->session()->push('errors', 'Missing file');
            return response()->json(['error' => 'Missing file'], 400);
        }
    }
}
