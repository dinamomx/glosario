<?php

namespace App\Http\Controllers\Admin;

use App\Termino;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTerminoRequest;
use App\Http\Resources\TerminoResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdateTerminoRequest;
use App\Events\TerminoWasUpdated;
use App\Events\TerminoWasDeleted;
use App\Helpers;

class TerminoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terminos = Termino::paginate(15);
        return view('admin.terminos.index', ['terminos' => $terminos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_termino = Termino::DEFAULT_ATTIRBUTES;

        return view('admin.terminos.create', ['termino' => array_merge($empty_termino, ['thumbnail_file' => null])]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateTerminoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTerminoRequest $request)
    {
        $validated = $request->validated();
        $html = $validated['description'];
        $html = Helpers::cleanHTML($html);
        $html = Helpers::minifyHTML($html);
        // $html = Helpers::replaceUrls($html);
        $validated['description'] = $html;
        $termino =  new Termino($validated);
        if ($request->file('thumbnail_file')) {
            try {
                $path = $request->file('thumbnail_file')->store('terminos_thumbnails', 'public');
                $termino->thumbnail_path = $path;
                $termino->thumbnail_url = Storage::url($path);
                $termino->save();
            } catch (Exception $e) {
                $request->session()->push('errors', 'No se pudo guardar la imagen');
                $request->session()->push('errors', $e->getMessage());
            }
        } else {
            $termino->save();
        }
        if (array_key_exists('related', $validated)) {
            $termino->relatedTo()->attach($validated['related']);
        }
        $request->session()->flash('message', 'Término creado con éxito!');

        event(new TerminoWasUpdated($termino));
        if ($request->ajax()) {
            return new TerminoResource($termino);
        }
        return redirect(route('admin.terminos.show', $termino));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Termino  $termino
     * @return \Illuminate\Http\Response
     */
    public function show(Termino $termino)
    {
        return redirect()->action('TerminoController@show', ['termino' => $termino]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Termino  $termino
     * @return \Illuminate\Http\Response
     */
    public function edit(Termino $termino)
    {
        $termino->load(['relatedTo']);
        return view('admin.terminos.edit', ['termino' =>  $termino]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Termino  $termino
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTerminoRequest $request, Termino $termino)
    {
        $validated = $request->validated();
        $html = $validated['description'];
        $html = Helpers::cleanHTML($html);
        $html = Helpers::minifyHTML($html);
        // $html = Helpers::replaceUrls($html);
        $validated['description'] = $html;
        $termino->update($validated);
        // Update thumb
        if ($request->file('thumbnail_file')) {
            if ($termino->thumbnail_path) {
                Storage::delete($termino->thumbnail_path);
            }
            try {
                $path = $request->file('thumbnail_file')->store('terminos_thumbnails', 'public');
                $termino->thumbnail_path = $path;
                $termino->thumbnail_url = Storage::url($path);
                $termino->save();
            } catch (Exception $e) {
                $request->session()->push('errors', 'No se pudo guardar la imagen');
                $request->session()->push('errors', $e->getMessage());
            }
        } elseif (blank($request->thumbnail_url)) {
            // Delete previus file only if exists
            if ($termino->thumbnail_path) {
                Storage::delete($termino->thumbnail_path);
            }
            $termino->thumbnail_path = null;
            $termino->thumbnail_url = null;
            $termino->save();
        } else {
            $termino->save();
        }
        if (array_key_exists('related', $validated)) {
            $termino->relatedTo()->sync($validated['related']);
        } else {
            $termino->relatedTo()->detach();
        }

        event(new TerminoWasUpdated($termino));
        if ($request->ajax()) {
            return new TerminoResource($termino);
        }
        $request->session()->flash('message', 'Termino actualizado con éxito!');
        return redirect(route('terminos.show', $termino));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Termino  $termino
     * @return \Illuminate\Http\Response
     */
    public function destroy(Termino $termino)
    {
        event(new TerminoWasDeleted($termino));
        $termino->delete();
        session()->flash('message', 'Termino eliminado con éxito');
        if (request()->ajax()) {
            return response()->json([]);
        }
        return redirect(route('admin.terminos.index'));
    }
}
