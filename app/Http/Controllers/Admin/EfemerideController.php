<?php

namespace App\Http\Controllers\Admin;

use App\Efemeride;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEfemerideRequest;
use App\Http\Resources\EfemerideResource;
use App\Http\Requests\UpdateEfemerideRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Events\EfemerideWasUpdated;
use App\Events\EfemerideWasDeleted;
use App\Helpers;

class EfemerideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $efemerides = Efemeride::paginate(15);
        return view('admin.efemerides.index', ['efemerides' => $efemerides]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_efemeride = Efemeride::DEFAULT_ATTIRBUTES;
        return view('admin.efemerides.create', ['efemeride' => array_merge($empty_efemeride, ['thumbnail_file' => null])]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Request\CreateEfemerideRequest  $request
     * @return \Illuminate\Http\RedirectResponse|App\Http\Resources\EfemerideResource
     */
    public function store(CreateEfemerideRequest $request)
    {
        $validated = $request->validated();
        $html = $validated['description'];
        $html = Helpers::cleanHTML($html);
        $html = Helpers::minifyHTML($html);
        // $html = Helpers::replaceUrls($html);
        $validated['description'] = $html;
        $validated['date'] = Carbon::parse($validated['date']);
        $efemeride = new Efemeride($validated);
        if ($request->file('thumbnail_file')) {
            try {
                $path = $request->file('thumbnail_file')->store('efemerides_thumbnails', 'public');
                $efemeride->thumbnail_path = $path;
                $efemeride->thumbnail_url = Storage::url($path);
                $efemeride->save();
            } catch (Exception $e) {
                $request->session()->push('errors', 'No se pudo guardar la imagen');
                $request->session()->push('errors', $e->getMessage());
            }
        } else {
            $efemeride->save();
        }
        // $efemeride->relatedTo()->attach($validated['related']);
        $request->session()->flash('message', 'Efemeride creado con éxito!');

        event(new EfemerideWasUpdated($efemeride));
        if ($request->ajax()) {
            return new EfemerideResource($efemeride);
        }
        return redirect(route('efemerides.show', $efemeride));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Efemeride  $efemeride
     * @return \Illuminate\Http\Response
     */
    public function show(Efemeride $efemeride)
    {
        return redirect()->action('EfemerideController@show', ['efemeride' => $efemeride]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Efemeride  $efemeride
     * @return \Illuminate\Http\Response
     */
    public function edit(Efemeride $efemeride)
    {
        return view('admin.efemerides.edit', ['efemeride' =>  $efemeride]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEfemerideRequest $request
     * @param  \App\Efemeride  $efemeride
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEfemerideRequest $request, Efemeride $efemeride)
    {
        $validated = $request->validated();
        $html = $validated['description'];
        $html = Helpers::cleanHTML($html);
        $html = Helpers::minifyHTML($html);
        // $html = Helpers::replaceUrls($html);
        $validated['description'] = $html;
        $validated['date'] = Carbon::parse($validated['date']);
        $efemeride->update($validated);
        // Update thumb
        if ($request->file('thumbnail_file')) {
            if ($efemeride->thumbnail_path) {
                Storage::delete($efemeride->thumbnail_path);
            }
            try {
                $path = $request->file('thumbnail_file')->store('efemerides_thumbnails', 'public');
                $efemeride->thumbnail_path = $path;
                $efemeride->thumbnail_url = Storage::url($path);
                $efemeride->save();
            } catch (Exception $e) {
                $efemeride->session()->push('errors', 'No se pudo guardar la imagen');
                $efemeride->session()->push('errors', $e->getMessage());
            }
        } elseif (blank($request->thumbnail_url)) {
            // Delete previus file only if exists
            if ($efemeride->thumbnail_path) {
                Storage::delete($efemeride->thumbnail_path);
            }
            $efemeride->thumbnail_path = null;
            $efemeride->thumbnail_url = null;
            $efemeride->save();
        } else {
            $efemeride->save();
        }
        event(new EfemerideWasUpdated($efemeride));
        if ($request->ajax()) {
            return new EfemerideResource($efemeride);
        }
        $request->session()->flash('message', '¡Efemeride actualizada con éxito!');
        return redirect(route('efemerides.show', $efemeride));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Efemeride  $efemeride
     * @return \Illuminate\Http\Response
     */
    public function destroy(Efemeride $efemeride)
    {
        event(new EfemerideWasDeleted($efemeride));
        $efemeride->delete();
        session()->flash('message', 'Efemeride eliminada con éxito');
        if (request()->ajax()) {
            return response()->json([]);
        }
        return redirect(route('admin.efemerides.index'));
    }
}
