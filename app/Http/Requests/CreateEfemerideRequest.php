<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CreateEfemerideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                   => 'required|string',
            'slug'                   => 'required|alpha_dash|unique:efemerides,slug',
            'date'                   => 'required|date',
            'description'            => 'required|string',
            'elements'               => 'array|nullable',
            'elements.*.insertType'  => [
                'required',
                'string',
                Rule::in(['iframe', 'media', 'url']),
            ],
            'elements.*.type'        => [
                'required',
                'string',
                Rule::in(['Audio', 'Video', 'Documento']),
            ],
            'elements.*.description' => 'required|string',
            'elements.*.iframe'      => 'required_if:elements.*.insertType,iframe|string|nullable',
            'elements.*.url'         => 'required_if:elements.*.insertType,url,media|url|nullable',
            'thumbnail_file'         => 'sometimes|image|nullable',
            'thumbnail_desc'         => 'sometimes|string|nullable',
            'display_featured_image' => 'boolean'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'elements.*.insertType' => [
                'required' => 'Necesitas seleccionar como quieres insertar el documento'
            ],
            'elements.*.type' => [
                'required' => 'Necesitas seleccionar que tipo de documento es'
            ],
            'elements.*.iframe' => [
                'required_if' => 'El iframe es obligatorio si :other'
            ],
        ];
    }

    /**
     * Especifica el nombre de los atributos
     *
     * @return array
     **/
    public function attributes()
    {
        return [
            'elements.*.insertType' => 'método de inserción',
            'elements.*.type' => 'tipo del adjunto',
            'elements.*.description' => 'descripción del adjunto',
            'elements.*.iframe' => 'código de inserción',
            'elements.*.url' => 'url del documento',
        ];
    }
}
