<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use App\Scopes\DateScope;

class Efemeride extends Model
{
    use SoftDeletes;
    use Searchable;

    /** @var String $table la tabla del modelo */
    public $table = 'efemerides';

     /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new DateScope);
    }

    const DEFAULT_ATTIRBUTES = [
        'name' => null,
        'slug' => null,
        'date' => null,
        'description' => null,
        // Estos estan de forma demostrativa, ya no los necesitamos
        // [
        //     'url' => null,
        //     'type' => 'Audio',
        //     'insertType' => 'iframe',
        //     'iframe' =>null,
        //     'description' => null,
        // ],
        // [
        //     'url' => null,
        //     'type' => 'Video',
        //     'insertType' => 'iframe',
        //     'iframe' => null,
        //     'description' => null,
        // ],
        // [
        //     'url' => null,
        //     'type' => 'Documento',
        //     'insertType' => 'iframe',
        //     'iframe' => null,
        //     'description' => null,
        // ]
        'elements' => "[]",
        'user_id' => null,
        'thumbnail_path' => null,
        'thumbnail_url' => null,
        'thumbnail_desc' => null,
        'display_featured_image' => true,
    ];

    protected $attributes = self::DEFAULT_ATTIRBUTES;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'date',
        'description',
        'elements',
        'user_id',
        'thumbnail_desc',
        'display_featured_image',
        'pdf_path',
        'pdf_url',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'date'];

    protected $casts = [
        'elements' => 'array',
        'display_featured_image' => 'boolean'
    ];

    protected $guarded = [
        'id',
        'user_id'
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        $array['elements'] = implode(
            ', ',
            array_filter(
                array_pluck($array['elements'], '*.description')
            )
        );
        return $array;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function thumbnail()
    {
        return $this->belongsTo(Thumbnail::class);
    }

    /**
     * El usuario que creo este termino
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

     /**
     * Get the user's first name.
     *
     * @param  string  $value
     * @return Collection
     */
    public function getElementsAttribute($value)
    {
        return collect(json_decode($value));
    }

    /**
     * Registra un hit en el modelo
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function hit()
    {
        $this->hits++;
        $this->save();
        return $this->hits;
    }


    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRanked($query)
    {
        return $query->orderBy('hits', 'desc');
    }
}
