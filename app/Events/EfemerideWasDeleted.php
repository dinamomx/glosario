<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use App\Efemeride;

class EfemerideWasDeleted
{
    use Dispatchable, SerializesModels;

    /**
     * La efemeride actualizada
     *
     * @var Efemeride
     */
    public $efemeride;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Efemeride $efemeride)
    {
        $this->efemeride = $efemeride;
    }
}
