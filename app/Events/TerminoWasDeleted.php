<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use App\Termino;

class TerminoWasDeleted
{
    use Dispatchable, SerializesModels;

    /**
     * El Termino actualizado
     *
     * @var Termino
     */
    public $termino;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Termino $termino)
    {
        $this->termino = $termino;
    }
}
