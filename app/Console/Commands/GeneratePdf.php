<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Efemeride;
use App\Termino;
use App\Events\EfemerideWasUpdated;
use App\Events\TerminoWasUpdated;

class GeneratePdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:pdf
                            {model? : El nombre del modelo a usar, Default: Terminos y Efemerides}
                            {--slug=* : Entradas especificas por slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera los pdfs de todos las entradas.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $modelos = $this->argument('model');
        if (is_null($modelos)) {
            $modelos = ['Termino', 'Efemeride'];
        } else {
            $modelos = [$modelos];
        }
        $slugs = $this->option('slug');
        dump([$modelos, $slugs]);
        foreach ($modelos as $modelo) {
            // Término o efemeride
            if ($modelo === 'Efemeride') {
                if (!empty($slugs)) {
                    foreach ($slugs as $slug) {
                        $efemeride = Efemeride::where('slug', $slug)->first();
                        $this->info("Generando pdf para $efemeride->slug");
                        event(new EfemerideWasUpdated($efemeride));
                    }
                } else {
                    $all_efemerides = Efemeride::all();
                    foreach ($all_efemerides as $efemeride) {
                        $this->info("Generando pdf para $efemeride->slug");
                        event(new EfemerideWasUpdated($efemeride));
                    }
                }
            } else if ($modelo === 'Termino') {
                if (!empty($slugs)) {
                    foreach ($slugs as $slug) {
                        $efemeride = Termino::where('slug', $slug)->first();
                        $this->info("Generando pdf para $efemeride->slug");
                        event(new TerminoWasUpdated($efemeride));
                    }
                } else {
                    $all_efemerides = Termino::all();
                    foreach ($all_efemerides as $efemeride) {
                        $this->info("Generando pdf para $efemeride->slug");
                        event(new TerminoWasUpdated($efemeride));
                    }
                }
            }
        }
    }
}
