<?php
namespace App;
use HTMLPurifier_Config;
use HTMLPurifier;
class Helpers
{
    /**
     * Minifica el html de una forma simple
     *
     * @param string $html
     * @return string
     */
    static function minifyHTML(string $html) : string
    {
        $search = array(
            '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        );

        $replace = array(
            '>',
            '<',
            '\\1',
            ''
        );

        $html = preg_replace($search, $replace, $html);
        return $html;
    }

    /**
     * Reemplaza las urls en las imagenes de una cadena html
     *
     * @param string $originalhtml
     * @param string $string_to_replace
     * @return string
     */
    static function replaceUrls(string $originalhtml, string $string_to_replace = null, string $replace_with = '$1', string $prepend_condition = 'src="') : string
    {
        // Asegurate de utf-8 por que es 2019 y php todavía no tiene soporte
        // para utf-8
        mb_internal_encoding('UTF-8');
        if (blank($string_to_replace)) {
            $string_to_replace = config('app.url');
        }
        $string_to_replace = preg_quote($string_to_replace, '/');
        $patron;
        if ($prepend_condition) {
            $patron = "/($prepend_condition)$string_to_replace/iu";
        } else {
            $patron = "/$string_to_replace/iu";
        }
        $html = preg_replace($patron, $replace_with, $originalhtml);
        return $html;
    }

    /**
     * Limpia el html ya sea usando tidy html o htmlpurifier.
     * Esto para prevenir ataques XSS y html mal formado
     *
     * @param string $html
     * @return string
     */
    static function cleanHTML(string $html) : string
    {
        return self::cleanWithPurifier($html);
        // if (class_exists('tidy')) {
        //     return self::cleanWithTidy($html);
        // } else {
        // }
    }

    /**
     * Limpieza con Tidy, se requiere la extensión tidy de php
     *
     * @param string $html
     * @uses tidy La clase tidy
     * @return string
     */
    private static function cleanWithTidy(string $html) : string
    {
        $tidy_config = array(
            'clean'                       => true,
            'drop-proprietary-attributes' => true,
            'output-xhtml'                => true,
            'show-body-only'              => true,
            'word-2000'                   => true,
            'wrap'                        => '0'
        );
        $tidy = new \tidy();
        $tidy->parseString($html, $tidy_config, 'utf8');
        $tidy->cleanRepair();
        return tidy_get_output($tidy);
    }

    /**
     * Limpia el html con HTMLPurifier no se requiere ninguna
     * librería externa.
     *
     * @param string $html
     * @return string
     */
    private static function cleanWithPurifier(string $html) : string
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8');
        $config->set('HTML.Doctype', 'HTML 4.01 Transitional');
        // Habilitar display
        $config->set('CSS.AllowTricky', true);
        $config->set('HTML.TargetBlank', true);
        $config->set('HTML.TargetNoopener', true);
        $config->set('HTML.TargetNoreferrer', false);
        $config->set('Attr.ID.HTML5', true);
        $config->set('AutoFormat.RemoveEmpty', true);
        $config->set('AutoFormat.RemoveSpansWithoutAttributes', true);
        $config->set('AutoFormat.RemoveEmpty.Predicate',
            ['colgroup' => [],
            'th' => [],
            'td' => [],
            'iframe' => [0 => 'src'],
            'p' => [],
            ]);
        // $config->set('HTML.AllowedAttributes', [
        //     '*.style'   => true,
        //     '*.class'   => true,
        //     'img.src'   => true,
        //     'img.alt'   => true,
        //     'img.width' => true,
        //     'a.href'    => true,
        //     'a.target'  => true,
        // ]);
        $config->set('Attr.AllowedFrameTargets', [
            '_blank'  => true,
            '_self'   => true,
            '_parent' => true,
            '_top'    => true,
        ]);
        $config->set('Cache.SerializerPath', storage_path('tmp/'));
        $purifier = new HTMLPurifier($config);
        $clean_html = $purifier->purify($html);
        return $clean_html;
    }
}
