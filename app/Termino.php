<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Builder;
use App\Scopes\AlphaScope;

class Termino extends Model
{
    use SoftDeletes;
    use Searchable;

    /** @var String $table la tabla del modelo */
    public $table = 'terminos';


     /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new AlphaScope);
    }


    const DEFAULT_ATTIRBUTES = [
        'name' => null,
        'slug' => null,
        'description' => null,
        // Estos estan de forma demostrativa, ya no los necesitamos
        // [
        //     'url' => null,
        //     'type' => 'Audio',
        //     'insertType' => 'iframe',
        //     'iframe' =>null,
        //     'description' => null,
        // ],
        // [
        //     'url' => null,
        //     'type' => 'Video',
        //     'insertType' => 'iframe',
        //     'iframe' => null,
        //     'description' => null,
        // ],
        // [
        //     'url' => null,
        //     'type' => 'Documento',
        //     'insertType' => 'iframe',
        //     'iframe' => null,
        //     'description' => null,
        // ]
        'elements' => "[]",
        'user_id' => null,
        'thumbnail_path' => null,
        'thumbnail_url'  => null,
        'thumbnail_desc' => null,
        'display_featured_image' => true,
    ];

    protected $attributes = self::DEFAULT_ATTIRBUTES;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'elements',
        'user_id',
        'thumbnail_desc',
        'display_featured_image',
        'pdf_path',
        'pdf_url',
    ];

    protected $casts = [
        'elements'               => 'array',
        'display_featured_image' => 'boolean',
    ];

    /**
     * Attributes that should not be masss assignable
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        $array['elements'] = implode(
            ', ',
            array_filter(
                array_pluck($array['elements'], '*.description')
            )
        );
        return $array;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Terminos relacionados
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relatedBy()
    {
        // return $this->belongsTo('App\Post', 'foreign_key', 'other_key');

        return $this->belongsToMany(Termino::class, 'related_terminos', 'child_termino_id', 'parent_termino_id');
    }

    /**
     * Terminos relacionados
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relatedTo()
    {
        return $this->belongsToMany(Termino::class, 'related_terminos', 'parent_termino_id', 'child_termino_id');
    }

    /**
     * El usuario que creo este termino
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Registra un hit en el modelo
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function hit()
    {
        $this->hits++;
        $this->save();
        return $this->hits;
    }

     /**
     * Asegura de que $model->elements sea una colección
     *
     * @param  string  $value
     * @return Collection
     */
    public function getElementsAttribute($value)
    {
        return collect(json_decode($value));
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        return $query->orderBy('name', 'asc')->get();
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRanked($query)
    {
        return $query->orderBy('hits', 'desc');
    }
}
