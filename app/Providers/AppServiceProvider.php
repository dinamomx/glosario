<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Carbon\Carbon as BaseCarbon;
use App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::resourceVerbs([
            'create' => 'crear',
            'edit' => 'editar',
        ]);
        Schema::defaultStringLength(191);
        $locale_set = setlocale(LC_ALL, 'es_MX.UTF8', 'es_MX.UTF-8', 'es_ES.UTF8', 'es_ES.UTF-8', 'es.UTF8', 'es.UTF-8');
        if (!$locale_set) {
            $e = new \Exception("Lenguaje español no está instalado.", 1);
            report($e);
        }
        App::setLocale('es');
        Paginator::defaultView('pagination::bulma');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
