<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Efemerides
        'App\Events\EfemerideWasUpdated' => [
            'App\Listeners\GenerateEfemeridePdf',
        ],
        'App\Events\EfemerideWasDeleted' => [
            'App\Listeners\DeleteEfemeridePdf',
        ],
        // Terminos
        'App\Events\TerminoWasUpdated' => [
            'App\Listeners\GenerateTerminoPdf',
        ],
        'App\Events\TerminoWasDeleted' => [
            'App\Listeners\DeleteTerminoPdf',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
