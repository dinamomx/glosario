<?php
namespace App\Listeners;

use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Helpers;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

abstract class AbstractGeneratePdf implements ShouldQueue
{
    use InteractsWithQueue, Dispatchable, Queueable, SerializesModels;

    /**
     * El nombre de la plantilla blade a usar
     *
     * @var String
     */
    const BLADE_TEMPLATE_NAME = 'pdf-template';

    /**
     * La ruta base del pdf
     *
     * @var String
     */
    public static $base_path = 'pdf_files';

    private $dompdf_options;

    public function __construct() {

        $this->queue = 'pdf';

        $opts = [
            'isRemoteEnabled' => true,
            'tempDir' => storage_path('tmp/'),
            'fontDir' => storage_path('fonts/'),
            'fontCache' => storage_path('fonts/'),
            'chroot' => realpath(storage_path()),
            'rootDir' => realpath(base_path()),
            'logOutputFile' => storage_path('logs/dompdf.htm'),
            'isHtml5ParserEnabled' => true,
            'debugKeepTemp' => false,
            'debugCss' => false,
            'debugLayout' => true,
            'debugLayoutLines' => false,
            'debugLayoutBlocks' => false,
            'debugLayoutInline' => false,
            'debugLayoutPaddingBox' => false,
        ];
        $this->dompdf_options = new Options($opts);
    }

    /**
     * GenerateHTML
     *
     * Genera el html y lo guarda basado en la plantilla para pdf
     *
     * @param ModelIdentifier $termino El término a renderizar
     * @return string
     **/
    public function renderHTML(Eloquent $model)
    {
        // Obtiene el nombre del modelo
        $model_name = strtolower(class_basename(get_class($model)));
        // Donde se guardará el html
        $html_path = "html_tempaltes/$model_name/$model->slug.html";

        // $html = Helpers::replaceUrls($model->description, '/');
        // Genera la vista en html usando la template
        $view = view(self::BLADE_TEMPLATE_NAME, [
            'document' => $model,
            'html' => $model->description,
            'document_url' => route('efemerides.show', $model),
            'site_url'=> url()->full(),
        ]);
        // Guardando el html como cache, se sobreescriben intentos anteriores
        Storage::disk('local')->put($html_path, $view);
        Storage::disk('public')->put($html_path, $view);
        logger('HTML generado', ['html_path' => $html_path]);
        // Llama la generación de pdf
        $pdf = $this->renderPDF($html_path, $model->slug);
        logger('PDF Creado', [$pdf]);
        // Guarda donde está el pdf en la db
        $model->fill($pdf);
        $model->save();
    }

    /**
     * Crea el pdf
     *
     * @param string $filePath La ruta al archivo html para leerlo
     * @return array La ruta y la url del pdf
     **/
    public function renderPDF(string $html_path, string $slug)
    {
        // instantiate and use the dompdf class
        $dompdf = new Dompdf($this->dompdf_options);

        $dompdf->setBasePath(realpath(public_path().'/'));

        $dompdf->loadHtmlFile(storage_path('app/public/'.$html_path));

        // Render the HTML as PDF
        $dompdf->render();

        $pdf_string = $dompdf->output();
        // Output the generated PDF to Browser
        $pdf_path = static::$base_path . "/$slug.pdf";

        Storage::disk('public')->put($pdf_path, $pdf_string);

        $pdf_url = Storage::url($pdf_path);

        return [
            'pdf_path' => $pdf_path,
            'pdf_url' => $pdf_url,
        ];
    }
}
