<?php

namespace App\Listeners;

use App\Events\TerminoWasUpdated as Event;
use Exception;
class GenerateTerminoPdf extends AbstractGeneratePdf
{
    public static $base_path = 'terminos_pdf';

    /**
     * Handle the event.
     *
     * @param  TerminoWasUpdated  $event
     * @return void
     */
    public function handle(Event $event)
    {
        // Access the order using $event->order...
        $termino = $event->termino;
        $slug = $event->termino->slug;
        logger('Creando termino pdf', ['slug' => $slug]);
        $this->renderHTML($termino);

    }

    /**
     * Handle a job failure.
     *
     * @param  \App\Events\TerminoWasUpdated  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(Event $event, Exception $exception)
    {
        // Access the order using $event->order...
        logger('error creando termino pdf', ['slug' => $event->termino->slug, 'exception' => $exception]);
    }

}
