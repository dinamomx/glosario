<?php

namespace App\Listeners;

use App\Events\EfemerideWasUpdated as Event;
use Exception;
class GenerateEfemeridePdf extends AbstractGeneratePdf
{
    public static $base_path = 'efemerides_pdf';

    /**
     * Handle the event.
     *
     * @param  EfemerideWasUpdated  $event
     * @return void
     */
    public function handle(Event $event)
    {
        // Access the order using $event->order...
        $efemeride = $event->efemeride;
        $slug = $event->efemeride->slug;
        logger('Creando efemeride pdf', ['slug' => $slug]);
        $this->renderHTML($efemeride);

    }

    /**
     * Handle a job failure.
     *
     * @param  \App\Events\EfemerideWasUpdated  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(Event $event, Exception $exception)
    {
        // Access the order using $event->order...
        logger('error creando efemeride pdf', ['slug' => $event->efemeride->slug, 'exception' => $exception]);
    }

}
