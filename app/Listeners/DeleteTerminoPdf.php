<?php

namespace App\Listeners;

use App\Events\TerminoWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class DeleteTerminoPdf implements ShouldQueue
{
    use InteractsWithQueue, Dispatchable, Queueable, SerializesModels;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TerminoWasDeleted  $event
     * @return void
     */
    public function handle(TerminoWasDeleted $event)
    {
        $termino = $event->termino;
        $base_path = GenerateTerminoPdf::$base_path;
        $pdf_path = $base_path . "/{$termino->slug}.pdf";
        Storage::disk('public')->delete($pdf_path);
        // Access the order using $event->order...
        return false;
    }
}
