<?php

namespace App\Listeners;

use App\Events\EfemerideWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class DeleteEfemeridePdf implements ShouldQueue
{
    use InteractsWithQueue, Dispatchable, Queueable, SerializesModels;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EfemerideWasDeleted  $event
     * @return void
     */
    public function handle(EfemerideWasDeleted $event)
    {
        $efemeride = $event->efemeride;
        $base_path = GenerateEfemeridePdf::$base_path;
        $pdf_path = $base_path  . "/{$efemeride->slug}.pdf";
        Storage::disk('public')->delete($pdf_path);
        // Access the order using $event->order...
        return false;
    }
}
