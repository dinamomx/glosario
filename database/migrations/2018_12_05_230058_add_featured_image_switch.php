<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeaturedImageSwitch extends Migration
{
    const COLUMN_NAME = 'display_featured_image';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('efemerides', function (Blueprint $table) {
            $table->boolean(self::COLUMN_NAME)->default(true);
        });
        Schema::table('terminos', function (Blueprint $table) {
            $table->boolean(self::COLUMN_NAME)->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('efemerides', function (Blueprint $table) {
            $table->dropColumn(self::COLUMN_NAME);
        });
        Schema::table('terminos', function (Blueprint $table) {
            $table->dropColumn(self::COLUMN_NAME);
        });
    }
}
