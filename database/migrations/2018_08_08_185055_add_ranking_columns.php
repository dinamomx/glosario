<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRankingColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('efemerides', function (Blueprint $table) {
            $table->bigInteger('hits')->default(0);
        });
        Schema::table('terminos', function (Blueprint $table) {
            $table->bigInteger('hits')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('efemerides', function (Blueprint $table) {
            $table->dropColumn('hits');
        });
        Schema::table('terminos', function (Blueprint $table) {
            $table->dropColumn('hits');
        });
    }
}
