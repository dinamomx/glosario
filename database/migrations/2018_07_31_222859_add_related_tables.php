<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('related_terminos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_termino_id')->unsigned()->index();
            $table->foreign('parent_termino_id')
                ->references('id')
                ->on('terminos')
                ->onDelete('cascade');
            $table->integer('child_termino_id')->unsigned()->index();
            $table->foreign('child_termino_id')
                ->references('id')
                ->on('terminos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('related_terminos');
        Schema::dropIfExists('related_efemerides');
    }
}
