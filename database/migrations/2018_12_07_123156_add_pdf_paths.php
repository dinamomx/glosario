<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPdfPaths extends Migration
{
    const PATH_COLUMN_NAME = 'pdf_path';
    const URL_COLUMN_NAME = 'pdf_url';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('efemerides', function (Blueprint $table) {
            $table->string(self::PATH_COLUMN_NAME)->default(true);
            $table->string(self::URL_COLUMN_NAME)->default(true);
        });
        Schema::table('terminos', function (Blueprint $table) {
            $table->string(self::PATH_COLUMN_NAME)->default(true);
            $table->string(self::URL_COLUMN_NAME)->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('efemerides', function (Blueprint $table) {
            $table->dropColumn(self::PATH_COLUMN_NAME)->default(true);
            $table->dropColumn(self::URL_COLUMN_NAME)->default(true);
        });
        Schema::table('terminos', function (Blueprint $table) {
            $table->dropColumn(self::PATH_COLUMN_NAME)->default(true);
            $table->dropColumn(self::URL_COLUMN_NAME)->default(true);
        });
    }
}
