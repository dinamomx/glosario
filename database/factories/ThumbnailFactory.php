<?php

use Faker\Generator as Faker;

$factory->define(App\Thumbnail::class, function (Faker $faker) {
    return [
        'description' => 'Random generated image',
        'path' => $faker->imageUrl($width = 640, $height = 480),
    ];
});
