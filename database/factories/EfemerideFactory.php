<?php

use Faker\Generator as Faker;

$factory->define(App\Efemeride::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(),
        'slug' => $faker->unique()->slug(),
        'date' => $faker->dateTime(),
        'description' => $faker->paragraphs(10, true),
        'elements' => [
            [
                'url' => 'https://soundcloud.com/steven-universeonsc/stronger-than-you-feat-estelle',
                'type' => 'Audio',
                'insertType' => 'iframe',
                'iframe' => '<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/195691927&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>',
                'description' => 'Stronger than you - Stelle',
            ],
            [
                'url' => 'https://youtu.be/clJk8a5q1Lo',
                'type' => 'Video',
                'insertType' => 'iframe',
                'iframe' => '<iframe width="854" height="480" src="https://www.youtube.com/embed/clJk8a5q1Lo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
                'description' => 'Amar como tu - Crewuniverse',
            ],
            [
                'url' => 'https://es.scribd.com/document/200181882/Serious-Steven-Final-Storyboard-6-11-2013',
                'type' => 'Documento',
                'insertType' => 'iframe',
                'iframe' => '<p  style=" margin: 12px auto 6px auto; font-family: Helvetica,Arial,Sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; display: block;">   <a title="View Serious Steven Final Storyboard 6_11_2013 on Scribd" href="https://www.scribd.com/document/200181882/Serious-Steven-Final-Storyboard-6-11-2013#from_embed"  style="text-decoration: underline;" >Serious Steven Final Storyboard 6_11_2013</a> by <a title="View StevenCrewniverse\'s profile on Scribd" href="https://es.scribd.com/user/239689294/StevenCrewniverse#from_embed"  style="text-decoration: underline;" >StevenCrewniverse</a> on Scribd</p><iframe class="scribd_iframe_embed" title="Serious Steven Final Storyboard 6_11_2013" src="https://www.scribd.com/embeds/200181882/content?start_page=1&view_mode=scroll&access_key=key-c9o4g9z401pyahc3re9&show_recommendations=true" data-auto-height="false" data-aspect-ratio="1.6419068736141906" scrolling="no" id="doc_57563" width="100%" height="600" frameborder="0"></iframe>',
                'description' => 'Serious Steven Final Storyboard',
            ],
        ],
        'thumbnail_url' => $faker->imageUrl(600, 500, 'cats'),
        'thumbnail_desc' => $faker->sentence(),
    ];
});
