<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // $this->call(EfemerideTableSeeder::class);
        // $this->call(TerminoTableSeeder::class);
        // $this->call(LoremSeeder::class);
    }
}
