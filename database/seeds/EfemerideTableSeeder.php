<?php

use Illuminate\Database\Seeder;
use App\Efemeride;
use Carbon\Carbon;
use App\Thumbnail;

class EfemerideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Efemeride::create([
            'name' => 'Creación del Instituto Nacional de las Mujeres',
            'slug' => str_slug('Creación del Instituto Nacional de las Mujeres'),
            'date' => Carbon::parse('12-01-2001'),
            'description' => '<p>El Instituto Nacional de las Mujeres fue creado el 12 de enero del 2001, como un organismo público descentralizado de la administración pública federal, con personalidad jurídica, patrimonio propio y autonomía técnica y de gestión para el cumplimiento de sus atribuciones, objetivos y fines. Según la Ley del Instituto Nacional de las Mujeres, es la primera institución creada para las mujeres mexicanas</p>
            <p>Dentro de las atribuciones del Instituto Nacional de las Mujeres se encuentran: promover, coordinar, ejecutar y dar seguimiento a las acciones y programas destinados a garantizar la igualdad de oportunidades y de trato entre hombres y mujeres, combatiendo la violencia y discriminación hacia las mujeres. </p>',
            'elements' => [],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
        Efemeride::create([
            'name' => 'Día de la Cero Discriminación',
            'slug' => str_slug('Día de la Cero Discriminación'),
            'date' => Carbon::parse('01-03-2013'),
            'description' => '<p>A partir del 1° de diciembre de 2013 la Asamblea General de las Naciones Unidas se conmemora el día la Cero Discriminación, donde se nos invita a recordar el derecho a la no discriminación por razones de edad, sexo, identidad de género, orientación sexual, discapacidad, raza, etnia, idioma, estado de salud (incluyendo el VIH), localización geográfica, estatus económico o migratorio, o por cualquier otra índole.</p>
            <p>En México el Capítulo primero de la Constitución empieza diciendo: “En los Estados Unidos Mexicanos todas las personas gozaran de los derechos humanos reconocidos en esta Constitución y en los tratados internacionales de los que el Estado Mexicano sea parte, así́ como de las garantías para su protección, cuyo ejercicio no podrá́ restringirse ni suspenderse, salvo en los casos y bajo las condiciones que esta Constitución establece.”</p>',
            'elements' => [],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
        Efemeride::create([
            'name' => 'Día Internacional de acción por la salud de las mujeres',
            'slug' => str_slug('Día Internacional de acción por la salud de las mujeres'),
            'date' => Carbon::parse('28-05-1987'),
            'description' => '<p>Este día es motivo para reconocer el escaso conocimiento que suele tenerse de la salud de las mujeres y poner en marcha soluciones que respondan a las necesidades actuales en toda su diversidad, como atención a mujeres con discapacidades diversas y/o aquellas con enfermedades de transmisión sexual.</p> <p>En México, las mujeres tienen derecho a tomar decisiones respecto a su cuerpo, sexualidad, salud reproductiva y bienestar en general. Sin embargo, aún hay instancias y leyes que castigan la interrupción del embarazo, de igual forma, la muerte materna en México se presenta con mayor frecuencia en entidades donde la desigualdad y el rezago social está más presente. Sumado a lo anterior, las mujeres según INEGI, mueren en el país por enfermedades endócrinas, nutricionales y metabólicas, así como por diabetes y tumores malignos. </p>',
            'elements' => [],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
        Efemeride::create([
            'name' => 'Día internacional de la juventud',
            'slug' => str_slug('Día internacional de la juventud'),
            'date' => Carbon::parse('12-07-2000'),
            'description' => '<p>A partir del año 2000, el 12 de agosto se conmemora como el día internacional de la juventud. El Programa de Acción Mundial para los Jóvenes (PAMJ) de la ONU, ha promovido el desarrollo de la juventud, proporcionando un marco legal internacional que impulsen su bienestar y mejores condiciones dentro de la sociedad.</p> <p>En México más de 30 millones de habitantes son jóvenes entre 15 y 29 años, lo que equivale al 25.7% de la población.</p>',
            'elements' => [],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
    }
}
