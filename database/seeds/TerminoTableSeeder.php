<?php

use Illuminate\Database\Seeder;
use App\Termino;
use App\Thumbnail;

class TerminoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $termino = Termino::create([
            'name' => 'Abuso sexual',
            'slug' => str_slug('Abuso sexual'),
            'description' => "
                <p>De acuerdo al Código Penal Federal en su artículo 260, comete el delito de abuso sexual quien ejecute en una persona, sin
                su consentimiento, o la obligue a ejecutar para sí o en otra persona, actos sexuales sin el propósito de llegar a la cópula.</p>

                <p>El abuso sexual como una forma de violencia, vulnera el ejercicio de los derechos humanos, sexuales o reproductivos y las
                    principales víctimas son niñas, niños y mujeres.</p>

                <p>“La violencia sexual contra niñas y mujeres es una de las manifestaciones más claras de los valores, normas y tradiciones
                    en una cultura patriarcal que alientan a los hombres a creer que tienen el derecho de controlar el cuerpo y la sexualidad
                    de las mujeres” (Conell, 2000; Dobash y Dobash, 1979; Gasman et al. 2006; Heise et al. 1999; Jewkes et al. 2002)</p>

                <p>Tuana y González (2011) refieren que el abuso sexual es un problema de poder. No se explica por una patología individual
                    del abusador, ni por una enfermedad social. El abuso sexual hacia un niño o niña por parte de su padre –por ejemplo- es
                    una situación de vulneración de sus derechos que se apoya en un contexto general de legitimación de la violencia de género
                    y generacional.</p>",
            'elements' => [
                [
                    'url' => 'http://www.oas.org/dsp/documentos/observatorio/violencia_sexual_la_y_caribe_2.pdf',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'Violencia sexual',
                ],
                [
                    'url' => 'https://www.colibri.udelar.edu.uy/jspui/bitstream/123456789/7527/1/tfg_analia_ferrari.pdf',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'tfg_analia_ferrari',
                ],
                [
                    'url' => 'https://dialnet.unirioja.es/descarga/articulo/4384483.pdf',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => '4384483',
                ],
            ],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
        $termino = Termino::create([
            'name' => 'Acoso Sexual',
            'slug' => str_slug('Acoso Sexual'),
            'description' => "
            <p>En el Artículo 13 del capítulo 2 de la Ley General de Acceso de las Mujeres a una vida Libre de violencia se define como:
            “Una forma de violencia en la que, si bien no existe la subordinación, hay un ejercicio abusivo de poder que conlleva a
            un estado de indefensión y de riesgo para la víctima, independientemente de que se realice en uno o varios eventos.” Dentro
            de esta Ley, el acoso sexual aparece como parte de la violencia laboral y docente.</p>

            <p>Como una manifestación de la violencia constituye una violación a los derechos humanos y al derecho a una vida libre de violencia
            y es un obstáculo para la igualdad de oportunidades entre mujeres y hombres en los espacios laborales y educativos.</p>

            <p>Si bien, no responde a las diferencias de poder objetivo en el ámbito laboral, sí responde a un ejercicio de poder basado
            en la jerarquía social dándose en un contexto de violencia contra las mujeres.</p>

            <p>Con el fin de homologar los procedimientos en las dependencias y entidades de la Administración Pública Federal se ha creado
            el Protocolo para la prevención, atención y sanción del hostigamiento sexual y acoso sexual.</p>

            <p>
            <strong>25 de Noviembre</strong>: Día Internacional de la Eliminación de la violencia contra las mujeres.</p>",
            'elements' => [
                [
                    'url' => 'https://www.gob.mx/sfp/videos/protocolo-para-prevenir-atender-y-sancionar-el-hostigamiento-y-acoso-sexual',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'Protocolo',
                ],
                [
                    'url' => 'http://cedoc.inmujeres.gob.mx/documentos_download/protocolo_coah.pdf',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'protocolo_coah',
                ],
            ],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
        $termino = Termino::create([
            'name' => 'Acciones afirmativas',
            'slug' => str_slug('Acciones afirmativas'),
            'description' => "
            <p>De acuerdo a la Ley General para Mujeres y Hombres son el conjunto de medidas de carácter temporal encaminadas a acelerar
              la igualdad de hecho entre mujeres y hombres.</p>

            <p>La Convención sobre la eliminación de todas las forma de discriminación contra la mujer (CEDAW por sus siglas en inglés)
              en el Artículo 4 señala: “La adopción por los Estados Partes de medidas especiales de carácter temporal encaminadas a acelerar
              la igualdad de facto entre el hombre y la mujer no se considerará discriminación en la forma definida en la presente Convención
              pero de ningún modo entrañará, como consecuencia, el mantenimiento de normas desiguales o separadas; estas medidas cesarán
              cunado se hayan alcanzado los objetivos de igualdad de oportunidad y trato.</p>

            <p>El Comité de la CEDAW en la recomendación No. 25 habla también realizar los cambios estructurales, sociales y culturales
              necesarios para corregir las formas y consecuencias pasadas y presentes de la discriminación contra la mujer, así como
              compensarlas, señalando que estas medidas son de carácter temporal.</p>

            <p>El objetivo es acortar y erradicar las brechas de desigualdad históricas que viven las mujeres, por ello las acciones que
              las favorezcan a ellas para lograr la compensación.</p>",
            'elements' => [
                [
                    'url' => 'http://www.un.org/womenwatch/daw/cedaw/text/sconvention.htm',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'sconvention',
                ],
                [
                    'url' => 'http://www.un.org/womenwatch/daw/cedaw/recommendations/General%20recommendation%2025%20(Spanish).pdf',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'womenwatch',
                ],
            ],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
        $termino = Termino::create([
            'name' => 'Brechas de Desigualdad',
            'slug' => str_slug('Brechas de Desigualdad'),
            'description' => "
            <p>De acuerdo a la Ley General para Mujeres y Hombres son el conjunto de medidas de carácter temporal encaminadas a acelerar
              la igualdad de hecho entre mujeres y hombres.</p>

            <p>La Convención sobre la eliminación de todas las forma de discriminación contra la mujer (CEDAW por sus siglas en inglés)
              en el Artículo 4 señala: “La adopción por los Estados Partes de medidas especiales de carácter temporal encaminadas a acelerar
              la igualdad de facto entre el hombre y la mujer no se considerará discriminación en la forma definida en la presente Convención
              pero de ningún modo entrañará, como consecuencia, el mantenimiento de normas desiguales o separadas; estas medidas cesarán
              cunado se hayan alcanzado los objetivos de igualdad de oportunidad y trato.</p>

            <p>El Comité de la CEDAW en la recomendación No. 25 habla también realizar los cambios estructurales, sociales y culturales
              necesarios para corregir las formas y consecuencias pasadas y presentes de la discriminación contra la mujer, así como
              compensarlas, señalando que estas medidas son de carácter temporal.</p>

            <p>El objetivo es acortar y erradicar las brechas de desigualdad históricas que viven las mujeres, por ello las acciones que
              las favorezcan a ellas para lograr la compensación.</p>",
            'elements' => [
                [
                    'url' => 'https://www.oecd.org/mexico/Gender2017-MEX-es.pdf',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'sconvention',
                ],
                [
                    'url' => 'https://expansion.mx/nacional/2017/11/01/la-brecha-de-genero-en-mexico-se-agudiza-a-su-peor-nivel-desde-2013',
                    'type' => 'Documento',
                    'insertType' => 'url',
                    'iframe' => '',
                    'description' => 'womenwatch',
                ],
                [
                    'url' => 'https://www.youtube.com/watch?v=TXFBrob7HIk',
                    'type' => 'Video',
                    'insertType' => 'iframe',
                    'iframe' => '<iframe width="1351" height="480" src="https://www.youtube.com/embed/TXFBrob7HIk?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>',
                    'description' => 'Barreras de género: Brecha salarial | #RutaInclusiva',
                ],
            ],
            'thumbnail_path' => '/images/placeholder/example.jpg',
            'thumbnail_url'  => '/images/placeholder/example.jpg',
            'thumbnail_desc' => 'placeholder',
        ]);
    }
}
