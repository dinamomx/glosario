<?php

use Illuminate\Database\Seeder;

class LoremSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 50)
        ->create()
        ->each(function ($u) {
            $u->efemerides()->saveMany(factory(App\Efemeride::class, 10)->make());
            $u->terminos()->saveMany(factory(App\Termino::class, 10)->make());
        });
    }
}
