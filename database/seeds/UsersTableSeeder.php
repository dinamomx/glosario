<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'SuperAdmin',
            'email' => 'cesar@wdinamo.com',
            'role' => 'superadmin',
            'password' => Hash::make('ciubu8Uo')
        ]);
        User::create([
            'name' => 'Admin',
            'email' => 'janaya@inmujeres.gob.mx',
            'role' => 'admin',
            'password' => Hash::make('ix6uCohm')
        ]);
    }
}
