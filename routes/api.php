<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        // Alredy taken care
        // 'prefix' => 'api',
        'as' => 'api.',
        'namespace' => 'API',
    ],
    function () {
        Route::middleware('auth:api')->get('/user', function (Request $request) {
            return $request->user();
        })->name('profile');

        Route::resource('terminos', 'TerminoController')->only([
            'index', 'show'
        ]);
        Route::resource('efemerides', 'EfemerideController')->only([
            'index', 'show'
        ]);
    }
);
