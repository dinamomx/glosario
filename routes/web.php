<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/search', 'HomeController@search')->name('search');

Route::put('efemerides/{efemeride}/hit', 'EfemerideController@hit')->name('efemerides.hit');
Route::get('efemerides/populares', 'EfemerideController@populares')->name('efemerides.populares');
Route::resource('efemerides', 'EfemerideController')->only([
    'index', 'show'
]);

Route::put('terminos/{termino}/hit', 'TerminoController@hit')->name('terminos.hit');
Route::get('terminos/populares', 'TerminoController@populares')->name('terminos.popular');
Route::resource('terminos', 'TerminoController')->only([
    'index', 'show'
]);

Auth::routes(['register' => false]);

Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'can:accessAdminpanel'],
        'as' => 'admin.',
        'namespace' => 'Admin',
    ],
    function () {

        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::post('/files', 'DashboardController@storeFile')
            ->name('files');
        Route::resources([
            'efemerides' => 'EfemerideController',
            'terminos' => 'TerminoController',
        ]);
    }
);
