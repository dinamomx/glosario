<?php


/**
 * Returns a string with words meant to be used as css classes for the body
 *
 * @return string
 */
function bodyClass()
{
    $body_classes = [
        str_replace('.', '-', \Route::currentRouteName()),
    ];
    $class = '';
    foreach (\Request::segments() as $segment) {
        if (is_numeric($segment) || empty($segment)) {
            continue;
        }
        $class .= ! empty($class) ? "-" . $segment : $segment;
        array_push($body_classes, $class);
    }
    return ! empty($body_classes) ? implode(' ', $body_classes) : null;
}
