@extends('layouts.dashboard')
@section('content')
<section class="section">
  <h1 class="title">Nuevo término</h1>
  @include('components.alerts', ['fixed' => false])
  <generic-form :model="laravel.$_termino"
    action="{{ route('admin.terminos.store') }}"
    method="POST"
    resource="terminos"
    enctype="multipart/form-data">
    @csrf
  </generic-form>
</section>

@prepend('scripts')
<script type="text/javascript">
  laravel.$_termino = Object.assign({!! json_encode($termino)!!}, {!! json_encode(session('_old_input'))!!});
</script>
@endprepend()
@endsection()
