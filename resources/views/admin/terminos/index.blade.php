@extends('layouts.dashboard')

@section('content')
<section class="section">
  <h1 class="title">Términos</h1>
  @include('components.alerts', ['fixed' => false])

  @if (empty($terminos))
      @include('components.empty')
  @else
  <table class="table is-hoverable is-fullwidth">
    <thead>
      <th>Título</th>
      <th>Relacionados</th>
      <th>Acciones</th>
    </thead>
    <tbody>
    @foreach ($terminos as $entry)
    <tr>
       <td>
        <a href="{{route('terminos.show', $entry)}}">{{$entry->name}}</a>
       </td>
       <td >
       <div class="tags">
        @foreach ($entry->relatedBy as $related)
          <a class="tag" href="{{route('terminos.show', $related)}}">{{$related->name}}</a>
        @endforeach
       </div>
       </td>
       <td>
       <div class="field has-addons">
         <div class="control">
            <a href="{{route('admin.terminos.edit', $entry)}}" class="button"><span>Editar</span></a>
          </div>
         <div class="control">
          <button
          type="button"
          class="button is-danger"
          href="{{route('admin.terminos.destroy', $entry)}}"
          @click="$confirmDeleteResource('{{route('admin.terminos.destroy', $entry)}}')"><span>Eliminar</span></button>
         </div>
       </div>
       </td>
    </tr>
    @endforeach
    </tbody>
  </table>
  @endif
  {{ $terminos->links() }}
</section>
@endsection()
