@extends('layouts.dashboard')
@section('content')

<section class="section">
  <div class="box has-text-centered">
    <h1 class="title ">Glosario</h1>
    <div class="columns is-vcentered">
      <div class="column">
        <p class="is-size-1 has-text-primary">
          {{ $terminos_count ?? '0' }}
        </p>
        <p>
          Términos en el glosario
        </p>
      </div>
      <div class="is-divider-vertical"></div>
      <div class="column">
        <p class="is-size-2">
          <a
          class="has-text-primary"
          href="{{ route('admin.terminos.create') }}">
            <fa-icon icon="plus">PLUS</fa-icon>
          </a>
        </p>
        <p>
          Término
        </p>
      </div>
    </div>
    <h1 class="title has-text-centered">Efemérides</h1>
    <div class="columns is-vcentered">
      <div class="column">
        <p class="is-size-1 has-text-primary">
          {{ $efemerides_count ?? '0' }}
        </p>
        <p>
          Efemérides
        </p>
      </div>
      <div class="is-divider-vertical"></div>
      <div class="column">
        <p class="is-size-2 has-text-primary">
          <a
          class="has-text-primary"
          href="{{ route('admin.efemerides.create') }}">
            <fa-icon icon="plus">PLUS</fa-icon>
          </a>
        </p>
        <p>
          Efeméride
        </p>
      </div>
    </div>
  </div>
</section>

@endsection
