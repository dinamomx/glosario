@extends('layouts.dashboard')

@section('content')
<section class="section">
  <h1 class="title">Efemérides</h1>
  @include('components.alerts', ['fixed' => false])

  @if (empty($efemerides))
      @include('components.empty')
  @else
  <table class="table is-hoverable is-fullwidth">
    <thead>
      <th>Título</th>
      <th>Fecha</th>
      <th>Acciones</th>
    </thead>
    <tbody>
    @foreach ($efemerides as $entry)
    <tr>
       <td>
        <a href="{{route('efemerides.show', $entry)}}">{{$entry->name}}</a>
       </td>
       <td >
       <div class="tags">
        <span class="tag">{{$entry->date->formatLocalized('%d %B')}}</span>
       </div >
       </td>
       <td>
       <div class="field has-addons">
        <div class="control">
        <a href="{{route('admin.efemerides.edit', $entry)}}" class="button"><span>Editar</span></a>
        </div>
        <div class="control">
        <button
          type="button"
          class="button is-danger"
          @click="$confirmDeleteResource('{{route('admin.efemerides.destroy', $entry)}}')"><span>Eliminar</span></button>
        </div>
       </div>
       </td>
    </tr>
    @endforeach
    </tbody>
  </table>
  @endif
  {{ $efemerides->links() }}
</section>
@endsection()
