@extends('layouts.dashboard')

@section('content')
<section class="section">
    <h1 class="title">Editar efeméride</h1>
    <!-- if there are creation errors, they will show here -->

    @include('components.alerts', ['fixed' => false])
    <generic-form
      :model="laravel.$_efemeride"
      action="{{route('admin.efemerides.update', $efemeride)}}"
      method="POST"
      enctype="multipart/form-data">
      @method('PUT')
      @csrf
      <input type="hidden" name="id" value="{{$efemeride->id}}">
    </generic-form>
</section>
@prepend('scripts')
<script type="text/javascript">
laravel.$_efemeride = Object.assign({!! json_encode($efemeride)!!}, {!! json_encode(session('_old_input'))!!});

</script>
@endprepend()
@endsection()
