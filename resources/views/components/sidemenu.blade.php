<aside class="dash-sidebar">
  <a
    href="{{route('home')}}"
    class="dash-sidebar__header">
    <p>Glosario para la gualdad</p>
    <p class="dash-sidebar__title">Consulta en línea</p>
  </a>
  <nav class="dash-sidebar__nav dash-sidebar-nav">
    <ul class="dash-sidebar-nav__list">
      <li class="dash-sidebar-nav__parent">Glosario
        <ul class="dash-sidebar-nav__list">
          <li class="dash-sidebar-nav__child">
            <a href="{{ route('admin.terminos.create') }}" class="dash-sidebar-nav__link">Nuevo Término</a>
          </li>
          <li class="dash-sidebar-nav__child">
            <a href="{{ route('admin.terminos.index') }}" class="dash-sidebar-nav__link">Todos los términos</a>
          </li>
        </ul>
      </li>
      <li class="dash-sidebar-nav__parent">Efemérides
        <ul class="dash-sidebar-nav__list">
          <li class="dash-sidebar-nav__child">
            <a href="{{route('admin.efemerides.create')}}" class="dash-sidebar-nav__link">Nueva efeméride</a>
          </li>
          <li class="dash-sidebar-nav__child">
            <a href="{{route('admin.efemerides.index')}}" class="dash-sidebar-nav__link">Todas las efemérides</a>
          </li>
        </ul>
      </li>
    </ul>
  </nav>
</aside>
