<article class="media article-excerpt article-excerpt--efemeride">
  @if($entry->thumbnail_url)
  <div class="media-left">
    <figure class="image is-circle is-square article-excerpt__image">
      <img src="{{asset($entry->thumbnail_url)}}" alt="{{$entry->thumbnail_desc}}">
    </figure>
  </div>
  @endif

  <div class="media-content">
    <h1 class="article-excerpt__title">{{$entry->name}}</h1>
    <h2 class="subtitle">{{$entry->date->formatLocalized('%d %B')}}</h2>

    <p>{{str_limit(strip_tags($entry->description), 60)}}</p>

    <a
      class="article-excerpt__link"
      href="{{route('efemerides.show', $entry)}}">Ver más...</a>
  </div>
</article>
