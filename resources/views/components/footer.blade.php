<footer class="footer layout-dashboard__footer" id="footer">
  <div class="container">
    <div class="columns is-vcentered">
      <div class="column footer__logos">
        <img src="{{asset('/images/logo-inmujeres-horizontal.svg.png')}}" alt="Logotipo de INMUJERES" class="footer__logo footer__logo--inmujeres">
      </div>
      <div class="column">
        <p class="has-text-centered is-size-7">
          <a href="https://www.gob.mx/inmujeres?ref={{url()->current()}}" target="_blank" rel="noopener noreferrer" class="has-text-black">INSTITUTO NACIONAL DE LAS MUJERES</a><br>
          © Algunos Derechos Reservados<br>
          Boulevard Adolfo López Mateos 3325. Col. San Jerónimo Lídice, C.P. 10200,<br> Del. Magdalena Contreras, CDMX, Tel. 01(55) 5322 4200
        </p>
      </div>
      <div class="column footer__logos">
        <img src="{{asset('/images/logo-gobierno-horizontal.svg.png')}}" alt="Logotipo del Gobierno Federal Mexicano" class="footer__logo">
      </div>
      <div class="is-clearfix"></div>
    </div>

  </div>
</footer>
