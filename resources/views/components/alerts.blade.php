@php
if (!isset($fixed)) {
  $fixed = false;
}
@endphp

@if ($errors->any())
  <div class="notification is-danger {{$fixed ? 'notification--fixed' : ''}}">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

@if (session('status'))
    <div class="notification is-success {{$fixed ? 'notification--fixed' : ''}}">
        {{ session('status') }}
    </div>
@endif

@if (session('message'))
    <div class="notification is-info {{$fixed ? 'notification--fixed' : ''}}">
        {{ session('message') }}
    </div>
@endif
