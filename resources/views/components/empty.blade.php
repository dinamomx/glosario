<div class="notification is-primary has-text-centered">
  <p>
    <span class="icon is large">
      <fa-icon icon="frown"></fa-icon>
    </span>
  </p>
  <p class="title">No se encontraron {{ isset($resource) ? $resource : 'entradas' }}</p>
</div>