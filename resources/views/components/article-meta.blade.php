{{-- Botones de compartir y editar --}}
@if (isset($article))
<div class="is-pulled-right articulo__meta is-hidden-print">
  <div class="buttons">
      @if (isset($edit_link) && Auth::check())
      <a href="{{$edit_link}}" class="button is-primary">Editar</a>
      @endif
      @if (!is_null($article->pdf_url))
      <button onclick="$pdf.print(this)"
         aria-label="Imprimir página"
         data-target="{{asset($article->pdf_url)}}"
         class="button is-circled is-primary">
        <span class="icon">
          <fa-icon icon="print"></fa-icon>
        </span>
      </button>
      <a href="{{asset($article->pdf_url)}}"
         class="button is-circled is-primary"
         aria-label="Descargar página"
         target="_blank">
        <span class="icon">
          <fa-icon icon="download"></fa-icon>
        </span>
      </a>
      @endif
      <a href="https://facebook.com/sharer.php?u={{urlencode(url()->current())}}"
        target="blank"
        class="button is-primary is-circled articulo__share"
        aria-label="Compartir en facebook">
        <span class="icon">
          <fa-icon :icon="['fab', 'facebook']">
            Compartir en facebook
          </fa-icon>
        </span>
      </a>
      <a class="twitter-share-button is-circled button is-primary articulo__share"
        href="https://twitter.com/intent/tweet?url={{urlencode(url()->current())}}&text={{$article->name}}&via=inmujeres"
        target="blank"
        aria-label="Compartir en twitter">
        <span class="icon">
          <fa-icon :icon="['fab', 'twitter']">
            Compartir en twitter
          </fa-icon>
        </span>
      </a>
    </div>
</div>
<div class="is-clearfix is-hidden-desktop util-space__top--1x"></div>
@else
<div class="tag is-danger">No tengo $article</div>
@endif
