@if (isset($elements))
@php
$grouped = $elements
  ->groupBy('type')
  ->sortBy(function ($items, $key) {
    return collect(['Documento', 'Video', 'Audio'])->search($key);
  });
@endphp
@if ($elements->isNotEmpty())
<h4 class="title is-5">Fuentes y/o referencias</h4>
<ul class="articulo-elements">
  @foreach ($grouped as $type => $items)
  <li class="articulo-elements__item">
    <strong class="articulo-elements__type">{{$type}}{{count($items) > 1 ? 's' : ''}}:</strong>
    <ul class="articulo-elements__content">
      @foreach ($items as $item)
      <li class="articulo-elements__content-item">
          <a href="{{$item->url}}" target="__blank">
              {{$item->description}}
              <span class="is-hidden">{{$item->url}}</span>
          </a>
          @if ($item->insertType === 'iframe')
            <figure class="iframe">
              {!! $item->iframe !!}
            </figure>
          @elseif ($item->insertType === 'media')
            @if ($type === 'Audio')
            <figure class="audio">
              <v-audio controls src="{{$item->url}}">
                Tu navegador no soporta el elemento <code>audio</code>.
              </v-audio>
            </figure>
            @elseif ($type === 'Video')
            <figure class="video">
              <video src="{{$item->url}}" controls>
                  Tu navegador no soporta el elemento <code>video</code>.
              </video>
            </figure>
            @endif
          @endif
        </li>
      @endforeach
    </ul>
  </li>
  @endforeach
</ul>
@endif
@else
<div class="notification is-danger">No tengo <code>$elements</code></div>
@endif
