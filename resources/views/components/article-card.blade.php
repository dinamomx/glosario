@if ($style === 'card')
<article class="card">
  @if ($entry->thumbnail_url)
    <a
    href="{{$route}}"
    class="card-image">
      <figure class="image is-4by3">
        <img src="{{asset($entry->thumbnail_url)}}" alt="{{$entry->thumbnail_desc}}">
      </figure>
    </a>
  @endif
  <div class="card-content">
    <h1 class="article-excerpt__title title is-4">{{$entry->name}}</h1>
    @if($type === 'efemeride')
    <h2 class="article-excerpt__date subtitle">{{$entry->date->formatLocalized('%d %B')}}</h2>
    @endif
    <p>{{str_limit(strip_tags($entry->description), 280)}}</p>
    <a
      class="article-excerpt__link"
      href="{{$route}}">Ver más...</a>
  </div>
</article>
@else
<article class="media article-excerpt article-excerpt--style-media article-excerpt--type-{{$type or 'default'}}">
  @if ($entry->thumbnail_url)
    <a
    href="{{$route}}"
    class="media-left">
      <figure class="image is-circle is-square article-excerpt__image"><img src="{{asset($entry->thumbnail_url)}}" alt="{{$entry->thumbnail_desc}}"></figure>
    </a>
  @endif
    <div class="media-content">
      <h1 class="article-excerpt__title title">
        <a href="{{$route}}">
          {{$entry->name}}
        </a>
      </h>
      @if($type === 'efemeride')
      <h2 class="article-excerpt__date subtitle">{{$entry->date->formatLocalized('%d %B')}}</h2>
      @endif
      <p>{{str_limit(strip_tags($entry->description), 120)}}</p>
      <a
      class="article-excerpt__link"
      href="{{$route}}">Ver más...</a>
    </div>
  </article>
@endif


