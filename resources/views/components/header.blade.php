<header class="header" id="header">
  <div class="header__column header__column--left header__title">
    @include('components.title')
  </div>
  <div class="header__column header__column--center header__search">
    <img src="{{asset('/images/logo-gobierno-horizontal.svg.png')}}" alt="Logotipo del Gobierno Federal Mexicano" class="is-hidden-screen image">
    <div ref="searchBar"
         class="header__search-box">
      <nav aria-hidden="true"
           class="util-space__bottom--7x is-hidden-desktop">
        <ul role="navigation">
          <li class="navigation__item">
            <a class="header__link"
                href="{{route('home')}}">Inicio</a>
          </li>
          <li class="navigation__item">
            <a class="header__link"
                href="{{route('terminos.index')}}">Glosario</a>
          </li>
          <li class="navigation__item">
            <a class="header__link"
                href="{{route('efemerides.index')}}">Efemérides</a>
          </li>
          <li class="navigation__item">
            <a aria-label="Lo más buscado"
                class="header__link"
                href="{{ route('terminos.popular') }}">+ Buscado</a>
          </li>
        </ul>
      </nav>
      @if (!request()->is('/'))
      <form
      id="main-search"
      action="{{route('search')}}"
      method="GET"
      class="field has-addons">
        <p class="control is-expanded">
          <input
            aria-label="Buscar"
            role="search"
            type="search"
            name="search"
            value="{{ old('search') }}"
            id="main-search-bar"
            placeholder="Buscar"
            class="input is-medium"
            class="input">
        </p>
        <p class="control">
          <button
            type="submit"
            class="button is-medium"
            class="button"
            aria-label="Buscar">
            <fa-icon icon="search"></fa-icon>
          </button>
        </p>
      </form>
      @endif
    </div>
  </div>
  <nav class="header__column header__column--right header__nav">
    <img src="{{asset('/images/logo-inmujeres-horizontal.svg.png')}}" alt="Logotipo de INMUJERES" class="is-hidden-screen image">
    <button class="button is-outlined header__search-toogle"
            @click="$refs.searchBar.classList.toggle('is-active')">
      <fa-icon icon="bars"></fa-icon>
    </button>
    <ul class="navigation is-hidden-touch"
        role="navigation">
     <li class="navigation__item">
       <a class="header__link"
          href="{{route('home')}}">Inicio</a>
     </li>
     <li class="navigation__item">
       <a class="header__link"
          href="{{route('terminos.index')}}">Glosario</a>
     </li>
     <li class="navigation__item">
       <a class="header__link"
          href="{{route('efemerides.index')}}">Efemérides</a>
     </li>
     <li class="navigation__item">
       <a aria-label="Lo más buscado"
          class="header__link"
          href="{{route('terminos.popular')}}">+ Buscado</a>
     </li>
    </ul>
  </nav>
</header>
@section('scripts')
<script>
(function (d) {
  function act(el) {
    el.classList.add('is-active')
  }
  var links = d.querySelectorAll('.header__link')
  var pn = d.location.pathname
  var pop = pn.indexOf('populares') !== -1
  var path = pn.replace(window.laravel.basePath, '/').split('/')[2] || '/'
  for (let i = 0; i < links.length; i++) {
    var link = links[i]

    var t = link.textContent
    if (t === '+ Buscado') {
      if (pop) {
        act(link)
      }
      continue
    }
    if (pop && t === 'Glosario') {
      continue
    }
    var hr = link.pathname.replace(window.laravel.basePath, '/').split('/')[2] || '/'
    if (hr === path) {
      act(link)
    }
  }
})(document)
</script>
@endsection()
