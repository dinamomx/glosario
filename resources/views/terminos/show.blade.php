@extends('layouts.default')

@section('content')
<v-tracker></v-tracker>
<main class="section articulo articulo--termino">
  <div class="container">
    <div class="columns">
      <article class="column is-8 articulo__content">
          @include('components.article-meta', ['article' => $termino, 'edit_link' => route('admin.terminos.edit', $termino)])
          <h1 class="title articulo__title is-size-1-desktop">{{$termino->name}}</h1>
          <div class="content">
            {!! $termino->description !!}
          </div>
          @if (!blank($termino->thumbnail_url) && $termino->display_featured_image)
          <figure class="image articulo__thumbnail">
            <img src="{{asset($termino->thumbnail_url)}}" alt="{{$termino->thumbnail_desc}}">
            @if ($termino->thumbnail_desc)
            <figcaption class="articulo__figcaption">{{$termino->thumbnail_desc}}</figcaption>
            @endif
          </figure>
          @endif
          <div class="terminos-nav">

            @if($prev)
              <a href="{{route('terminos.show', $prev)}}" class="button is-pulled-left is-primary">
                <span class="icon"><fa-icon icon="arrow-left"></fa-icon></span>
                <span> {{$prev->name}} </span>
              </a>
            @endif

            @if($next)
              <a href="{{route('terminos.show', $next)}}"  class="button is-pulled-right is-primary">
                <span>{{$next->name}}</span>
                <span class="icon"><fa-icon icon="arrow-right"></fa-icon></span>
              </a>
            @endif
            <div class="clearfix"></div>
          </div>
      </article>
      <aside class="column articulo__side">
        <div class="is-clearfix"></div>
        @include('components.article-elements', ['elements' => $termino->elements])
        <br>
        @if ($termino->relatedTo->isNotEmpty() || $termino->relatedBy->isNotEmpty())
        <h4 class="title is-5">Términos relacionados</h4>
        <ul>
          @foreach ($termino->relatedTo as $related)
          <li><a href="{{route('terminos.show', $related)}}">{{$related->name}}</a></li>
          @endforeach
          @foreach ($termino->relatedBy as $related)
          <li><a href="{{route('terminos.show', $related)}}">{{$related->name}}</a></li>
          @endforeach
        </ul>
        @endif
      </aside>
    </div>
  </div>
</main>
@endsection()
