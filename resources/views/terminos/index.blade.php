@extends('layouts.default')

@section('content')
<section class="section">
  <div class="container">
    <h1 class="title">Términos</h1>
    <div class="tabs is-centered is-boxed is-medium">
      <ul>
        @if(str_contains(url()->current(), 'populares'))
        <li class=""><a href="{{ route('terminos.index') }}">Alfabético</a></li>
        <li class="is-active"><a href="{{ route('terminos.popular') }}">Popularidad</a></li>
        @else
        <li class="is-active"><a href="{{route('terminos.index')}}">Alfabético</a></li>
        <li class=""><a href="{{route('terminos.popular')}}">Popularidad</a></li>
        @endif
      </ul>
    </div>
    <div class="columns is-multiline">

      @forelse ($terminos->chunk(10) as $chunk)

        <div class="column is-3">
          <ul class="is-list">
          @foreach($chunk as $entry)
            <li><a href="{{route('terminos.show', $entry)}}">{{$entry->name}}</a></li>
          @endforeach
          </ul>
        </div>
        @empty
          @include('components.empty')
      @endforelse

    </div>
    {{ $terminos->links() }}
  </div>
</section>
@endsection()
