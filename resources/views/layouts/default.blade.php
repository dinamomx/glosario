<!DOCTYPE html>
<html lang="es">
  <head>
    {!! SEO::generate() !!}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-134449214-1');
    </script>
    @include('layouts.shared.head')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134449214-1"></script>
    <link href="{{ asset('css/layouts/default.css') }}" rel="stylesheet">
  </head>
  <body class="layout-default body {{bodyClass()}}">
  <div id="__app" class="layout-default app">
      @include('components.alerts', ['fixed' => true])
      @include('components.header')
      @yield('content')
    </div>
    @include('components.footer')
    {{-- Auth --}}
    @if (!Auth::guest())
      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="meta-admin">
        <div class="field has-addons">
          <div class="control">
            <a class="button is-small is-static">Administrador: </a>
          </div>
          <div class="control">
            <a href="{{ route('admin.dashboard') }}" class="button is-small is-info">Ir al panel</a>
          </div>
          <div class="control">
            <a href="{{ route('logout') }}"
              class="button is-small is-danger"
              onclick="event.preventDefault();document.getElementById('logout-form').submit();">
              Cerrar sesión
            </a>
          </div>
        </div>
        {{ csrf_field() }}
      </form>
    @endif
    <!-- Scripts -->
    <script>
      var SERVER_DATE = new Date("{{now()}}")
      var CLIENT_DATE = new Date()
      if (CLIENT_DATE.toDateString() !== SERVER_DATE.toDateString()) {
        console.warn('El servidor y el cliente no entienden la misma fecha')
      }
    </script>
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/public.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')

  </body>
</html>
