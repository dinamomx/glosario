<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/favicon.ico">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/global.css') }}" rel="stylesheet">

@stack('styles')
<script type="text/javascript">

  var laravel = {
    baseUrl: @json(url('/')),
    basePath: @json(route('home', null, false)),
  }
  window.laravel = laravel
</script>
@stack('scripts')
