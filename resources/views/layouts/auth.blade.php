<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Glosario para la igualdad</title>
    <meta name="description" content="Iniciar sesión">
    <meta name="robots" content="noindex,nofollow">
    @include('layouts.shared.head')
    <link href="{{ asset('css/layouts/auth.css') }}" rel="stylesheet">

  </head>
  <body class="layout-default">
    <div id="__app" class="layout-auth">
      @component('components.alerts', ['fixed' => true])
      @endcomponent()
      <div class="hero is-fullheight is-grey">
        <div class="hero-body">
          @yield('content')
        </div>
        <div class="hero-foot">
          @component('components.footer')
        </div>
      </div>
      @endcomponent()
    </div>
    <!-- Scripts -->
    @yield('scripts')
  </body>
</html>
