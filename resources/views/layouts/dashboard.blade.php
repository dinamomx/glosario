<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Glosario para la igualdad - Dashboard</title>
    <meta name="description" content="Panel de administración">
    <meta name="robots" content="noindex,nofollow">
    @include('layouts.shared.head')
    <link href="{{ asset('css/layouts/dashboard.css') }}" rel="stylesheet">
  </head>
  <body class="layout-default">
    <div id="__app" class="layout-dashboard">
      @component('components.sidemenu', ['class' => 'layout-dashboard'])
      @endcomponent()
      <header class="layout-dashboard__header dash-header ">
        @if (Auth::guest())
          <a class="dash-header__link" href="{{ route('login') }}">Login</a>
          <a class="dash-header__link" href="{{ route('register') }}">Register</a>
        @else
          <a class="dash-header__link" href="{{ route('home') }}">Hola {{ Auth::user()->name }}</a>
          <a class="dash-header__link" href="{{ route('logout') }}"
            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Salir
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST"
                style="display: none;">
              {{ csrf_field() }}
          </form>
        @endif
      </header>
      <main class="layout-dashboard__content">
        @yield('content')
      </main>
      @component('components.footer')
      @endcomponent()
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
