@inject('helpers', 'App\Helpers')
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="es">

<head>
  <title>{{$document->name}}</title>
  <meta http-equiv="Content-Type"
        content="text/html; charset=utf-8" />
  <link rel="stylesheet"
        href="{{public_path('css/pdf.css')}}">
</head>

<body>
  <div class="header"
       id="header">
    <table>
      <tbody>
        <tr>
          <td valign="middle" class="title">
            <p class="app-title">
              <strong>Glosario para la igualdad</strong><br>
              Consulta en línea
            </p>
          </td>
          <td valign="middle" class="logo">
            <img src="{{public_path('images/logo-gobierno-horizontal.svg.png')}}"
                 alt="Gobierno federal"
                 class="logo-gobierno">
          </td>
          <td valign="middle" class="logo">
              <img src="{{public_path('images/logo-inmujeres-horizontal.svg.png')}}"
                    alt="Inmujeres"
                    class="logo-inmujeres">
          </td>
        </tr>
      </tbody>
    </table>


  </div>
  {{-- Termina header --}}
  <div class="clearfix"></div>
  {{-- Empieza contenido de página --}}
  <div class="page">
    <h1>{{$document->name}}</h1>
    {{-- Fecha de efemeride --}}
    @if ($document->date)
      <h2>{{$document->date->formatLocalized('%d %B')}}</h2>
    @endif
    {{-- Imagen destacada --}}
    @if (!blank($document->thumbnail_url) && $document->display_featured_image)
    <div class="featured-image" align="center">
      <figure class="image">
        <img src="{{preg_replace("/^\//iu", '', $document->thumbnail_url)}}" alt="{{$document->thumbnail_desc}}">
        @if ($document->thumbnail_desc)
          <figcaption>{{$document->thumbnail_desc}}</figcaption>
        @endif
      </figure>
    </div>
    @endif
    {{-- Termina imagen destacada --}}
    <div class="content">
      {!! $html !!}
    </div>
    {{-- Empieza elementos --}}
    @if ($document->elements->isNotEmpty())
    <hr>
    @endif
    <div class="elements">
        @include('components.article-elements', ['elements' => $document->elements, 'iframes' => false])
    </div>
  </div>
  {{-- Empieza footer --}}
  <div id="footer"
       class="footer">
    <table>
      <tbody>
        <tr>
          <td>
            <p>
              {{-- FIX: Verificar url de admin y poner solo la url del término --}}
              <a href="{{$document_url}}">{{$document_url}}</a><br>
              {{-- <a href="{{$site_url}}">Glosario para la igualdad {{$site_url}}</a> --}}
            </p>
          </td>
          <td valign="middle">
            <p class="page-number"></p>
          </td>
        </tr>
      </tbody>
    </table>

  </div>
</body>

</html>
