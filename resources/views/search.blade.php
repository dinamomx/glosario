@extends('layouts.default')

@section('content')

<section class="section is-medium">
  <div class="container">
    <h1 class="title">Resultados de búsqueda</h1>
    <nav class="tabs is-centered is-medium">
      <ul>
        <li v-bind:class="{'is-active' : laravel.tabs === 'terminos'}"><a href="#terminos-tab"
           v-on:click.prevent="laravel.tabs = 'terminos'">Términos</a></li>
        <li v-bind:class="{'is-active' : laravel.tabs === 'efemerides'}"><a href="#efemerides-tab"
          v-on:click.prevent="laravel.tabs = 'efemerides'">Efemérides</a></li>
      </ul>
    </nav>
    <div id="terminos-tab" v-show="laravel.tabs === 'terminos'">
      @if ($terminos->isNotEmpty())
      <h2 class="title is-3">Términos encontrados</h2>
      <div class="columns is-multiline">
        @foreach ($terminos as $entry)
          <div class="column is-6 is-4-widescreen">
          @include('components.article-card',
          [
            'entry' => $entry,
            'route' => route('terminos.show', $entry),
            'style' => 'card',
            'type'  => 'termino'
          ])
          </div>
        @endforeach
      </div>
      {{ $terminos->links() }}
      @else
        <div class="notification has-text-centered">
          <p><span class="icon is-large">
              <fa-icon icon="sad-tear" size="7x"></fa-icon>
            </span></p>
          <p>No se encontraron Términos</p>
        </div>
      @endif
    </div>
    <div id="efemerides-tab" v-show="laravel.tabs === 'efemerides'">
      {{-- efemerides --}}
      @if ($efemerides->isNotEmpty())
      <h2 class="title is-3">
        Efemérides encontradas
      </h2>
      <div class="columns is-multiline">
        @foreach ($efemerides as $entry)
        <div class="column is-6 is-4-widescreen">
        @include('components.article-card',
        [
          'entry' => $entry,
          'route' => route('efemerides.show', $entry),
          'style' => 'card',
          'type'  => 'efemeride'
        ])
        </div>
        @endforeach
      {{ $efemerides->links() }}
      @else
        <div class="notification has-text-centered">
          <p><span class="icon is-large">
              <fa-icon icon="sad-tear" size="7x"></fa-icon>
            </span></p>
          <p>No se encontraron Efemérides</p>
        </div>
      @endif
    </div>
  </div>
</section>
<script type="text/javascript">
  window.laravel.tabs = 'terminos'
</script>
@endsection()
