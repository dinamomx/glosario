@extends('layouts.default')
@section('content')

<div class="hero is-warning is-fullheight-with-navbar" style="margin-bottom: -6rem;">
  <div class="hero-body has-text-centered">
    <div class="container">
      <h1 class="title is-1" style="font-family: sans-serif">Error 403, <br>
        acceso denegado.</h1>
      <p><a class="button is-white" href="{{url('/')}}">Regresar al inicio</a></p>
    </div>
  </div>
</div>

@endsection()
