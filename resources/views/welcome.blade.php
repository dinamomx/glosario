@extends('layouts.default')

@section('content')
<main class="hero is-fullheight">
  <div class="hero-body">
    <div class="container">
      <div class="is-flex util-flex__center">
        @include('components.title')
      </div>
      <h3 class="title has-text-centered is-4 util-space__top--1x">INMUJERES</h3>
      <p class="is-lead">
        El <strong><em>Glosario para la Igualdad</em></strong> es una herramienta en línea de libre acceso creada por el Inmujeres, para que, al realizar tus labores como servidor o servidora pública, tengas a la mano la consulta y revisión de conceptos de uso recurrente e indispensables en el diseño y formulación de acciones y políticas para la igualdad entre mujeres y hombres.<br />
        Cada noción incluye su definición y materiales como ligas de más referencias, vídeos en algunos casos las efemérides que correspondan y la opción de imprimir o compartir vía redes.<br />
        El Inmujeres seguirá brindando herramientas para acompañar y fortalecer el trabajo que desde el gobierno federal realizamos con miras a lograr una sociedad con igualdad.<br />
        Para navegar en el <strong><em>Glosario</em></strong> y consultar los términos de tu interés lo puedes hacer de las siguientes formas:
      </p>
      <h3 class="title is-4 util-space__top--1x">Formas de consulta</h3>

      <ul class="is-lead-list">
        <li>Consultar el <a href="{{ route('terminos.index') }}">listado de términos</a></li>
        <li>Navegar en el <a href="{{ route('efemerides.index') }}">listado cronológico de efemérides</a></li>
        <li>Revisar los <a href="{{ route('terminos.popular') }}">términos más buscados</a></li>
        {{-- <li>Durante todo el sitio puedes usar el <a href="#" data-focus="#main-search">buscador de términos y efemérides</a></li> --}}
      </ul>
      <hr>
      @if ($today_efemeride->isNotEmpty())
      {{-- Vacius --}}
        <h2 class="subtitle is-4">
          Un día como hoy...
        </h2>
        @foreach ($today_efemeride as $efemeride)
        @include('components.article-card',
        [
          'entry' => $efemeride,
          'route' => route('efemerides.show', $efemeride),
          'style' => 'media',
          'type'  => 'efemeride'
        ])
        @endforeach
        <hr>
      @endif
      <h2 class="subtitle is-4">Próximas efemérides...</h2>
      @forelse ($top_efemerides as $entry)
        @include('components.article-card',
        [
          'entry' => $entry,
          'route' => route('efemerides.show', $entry),
          'style' => 'media',
          'type'  => 'efemeride'
        ])
        @empty
        @include('components.empty')
      @endforelse
    </div>
  </div>
</main>
@endsection()
@section('scripts')
<script type="text/javascript">
(function (d) {
  /**
   * @description Hace focus en un elemento y le da la clase is-active
   * @param {HTMLElement|String} el El elemento a focusear
   */
  function focusElement(el) {
    var e = el
    if (typeof el === 'string') {
      e = document.querySelector(el)
    }
    var input = e.querySelector('input')
    function cleanClasses() {
      e.classList.remove('animated')
    }
    input.focus()
    input.classList.add('is-success')
    e.classList.add('bounce', 'animated', 'infinite')
    input.addEventListener('click', cleanClasses, false)
    input.addEventListener('input', cleanClasses, false)
    input.addEventListener('change', cleanClasses, false)
  }
  var allFocus = document.querySelectorAll('[data-focus]')
  for (let i = 0; i < allFocus.length; i++) {
    const element = allFocus[i];
    element.addEventListener('click', function (event) {
      event.preventDefault()
      focusElement(element.dataset.focus)
    })
  }
})(document)
</script>

@endsection()
