@extends('layouts.default') @section('content')
<v-tracker></v-tracker>
<main class="section articulo--efemeride">
  <div class="container">
    <div class="columns is-8 is-variable">
      <article class="column is-9 articulo__content">
        @include('components.article-meta', ['article' => $efemeride, 'edit_link' => route('admin.efemerides.edit', $efemeride)])
        {{-- <h2 class="title is-6 is-size-3-desktop is-marginless has-text-primary is-hidden-print">{{$efemeride->date->format('Y')}}</h2> --}}
        <h2 class="title is-4 is-size-3-desktop is-marginless is-hidden-screen">{{$efemeride->date->formatLocalized('%d %B')}}</h2>
        <h1 class="title articulo__title is-size-1-desktop">{{$efemeride->name}}</h1>
        <div class="content">
          {!! $efemeride->description !!}
        </div>
      </article>
      <aside class="column is-3 articulo__side">
        <h3 class="title articulo__date is-hidden-print">{{$efemeride->date->formatLocalized('%d %B')}}</h3>
        @if (!blank($efemeride->thumbnail_url) && $efemeride->display_featured_image)
        <figure class="image articulo__thumbnail">
          <img src="{{asset($efemeride->thumbnail_url)}}" alt="{{$efemeride->thumbnail_desc}}">
          <figcaption class="articulo__figcaption">
            {{$efemeride->thumbnail_desc}}
          </figcaption>
        </figure>
        @endif
        @include('components.article-elements', ['elements' => $efemeride->elements])
      </aside>
    </div>
  </div>
</main>
{{-- Efemérides relacionadas --}}
@if ($related->isNotEmpty())
<hr>
<section class="section is-hidden-print">
  <div class="container">
    <h4 class="title has-text-centered">Otras efemérides de este mes…</h4>
    <div class="columns is-multiline">
        @foreach ($related as $entry)
        <div class="column is-6">
            @include('components.article-card',
            [
              'entry' => $entry,
              'route' => route('efemerides.show', $entry),
              'style' => 'media',
              'type'  => 'efemeride'
            ])
        </div>
        @endforeach
    </div>
  </div>
</section>
@endif
@endsection ()
