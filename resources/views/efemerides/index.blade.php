@extends('layouts.default')

@section('content')
<section class="section">
  <div class="container">
    <h1 class="title">
      Efemérides
      @if(str_contains(url()->current(), 'populares'))
      <small class="has-text-weight-light">más populares</small>
      <a href="{{route('efemerides.index')}}" class="button is-pulled-right is-hidden-mobile">Ver en orden cronológico</a>
      @else
      <small class="has-text-weight-light">en orden cronológico</small>
      <a href="{{route('efemerides.populares')}}" class="button is-pulled-right is-hidden-mobile">Ver las más populares</a>
      @endif
    </h1>
    <p class="has-text-right">
      @if(str_contains(url()->current(), 'populares'))
      <a href="{{route('efemerides.index')}}" class="button  is-hidden-tablet">Ver en orden cronológico</a>
      @else
      <a href="{{route('efemerides.populares')}}" class="button is-hidden-tablet">Ver las más populares</a>
      @endif
    </p>
    <div class="columns is-multiline util-space__top--2x">
      @forelse ($efemerides as $entry)
        <div class="column is-6 is-6-widescreen">
          @include('components.article-card',
          [
            'entry' => $entry,
            'route' => route('efemerides.show', $entry),
            'style' => 'media',
            'type'  => 'efemeride'
            ])
            </div>
          @empty
          @include('components.empty')
      @endforelse
    </div>
    {{ $efemerides->links() }}

  </div>
</section>
@endsection()
