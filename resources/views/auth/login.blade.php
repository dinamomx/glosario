@extends('layouts.auth')

@section('content')
<div class="box">
  @include('components.title')
  <form class="login-form" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="field util-space--top">
      <label class="label">{{__('E-Mail Address')}}</label>
      <p class="control">
        <input class="input" id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
      </p>
      @if ($errors->has('email'))
      <p class="help is-danger">
        {{ $errors->first('email') }}
      </p>
      @endif
    </div>

    <div class="field">
      <label class="label">{{__('Password')}}</label>
      <p class="control">
        <input class="input" id="password" type="password" name="password" required>
      </p>
      @if ($errors->has('password'))
      <p class="help is-danger">
        {{ $errors->first('password') }}
      </p>
      @endif
    </div>

    <div class="field">
      <p class="control">
        <label class="checkbox">
          <input type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}> {{__('Remember Me')}}
        </label>
      </p>
    </div>
    <div class="field">
      <div class="control has-text-centered">
        <button type="submit" class="button is-white">{{__('Login')}}</button>
      </div>
    </div>
    {{-- <div class="field">
      <p class="control has-text-centered">
          <a
            class="button is-white is-outlined is-small"
            href="{{ route('password.request') }}">
            {{__('Forgot Your Password?')}}
          </a>
      </p>
    </div> --}}
  </form>
</div>
@endsection()