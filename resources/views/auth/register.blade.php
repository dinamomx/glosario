@extends('layouts.auth') @section('content')

<div class="box">
  @include('components.title')

  <form class="register-form util-space__top--6x" method="POST" action="{{ route('register') }}">

    {{ csrf_field() }}
    <div class="field">
      <label class="label">{{__('Name')}}</label>
      <p class="control">
        <input class="input" id="name" type="name" name="name" value="{{ old('name') }}" required autofocus>
      </p>

      @if ($errors->has('name'))
      <p class="help is-danger">
        {{ $errors->first('name') }}
      </p>
      @endif
    </div>

    <div class="field">
      <label class="label">{{__('E-Mail Address')}}</label>
      <p class="control">
        <input class="input" id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
      </p>

      @if ($errors->has('email'))
      <p class="help is-danger">
        {{ $errors->first('email') }}
      </p>
      @endif
    </div>

    <div class="field">
      <label class="label">{{__('Password')}}</label>
      <p class="control">
        <input class="input" id="password" type="password" name="password" required>
      </p>

      @if ($errors->has('password'))
      <p class="help is-danger">
        {{ $errors->first('password') }}
      </p>
      @endif
    </div>
    <div class="field">
      <label class="label">{{__('Confirm Password')}}</label>
      <p class="control">
        <input class="input" id="password-confirm" type="password" name="password_confirmation" required>
      </p>
    </div>
    <div class="field">
      <div class="control">
        <button type="submit" class="button is-primary">{{__('Register')}}</button>
      </div>
    </div>
  </form>
</div>
@endsection