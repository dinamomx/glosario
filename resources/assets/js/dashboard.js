import {
  library,
} from '@fortawesome/fontawesome-svg-core'
import {
  faSearch,
  faPlus,
  faTags,
  faFileDownload,
  faExclamationCircle,
  faAngleLeft,
  faAngleRight,
} from '@fortawesome/free-solid-svg-icons'
import {
  FontAwesomeIcon,
} from '@fortawesome/vue-fontawesome'
import Vue from 'vue'
import GenericForm from './components/GenericForm.vue'
import axios from './axios'

import Dialog from '../lib/buefy/src/components/dialog/Dialog.vue'
import Loading from '../lib/buefy/src/components/loading/Loading.vue'

import Checkbox from '../lib/buefy/src/components/checkbox/Checkbox.vue'
// import { Dropdown, DropdownItem } from '../lib/buefy/src/components/dropdown'
// import { Radio, RadioButton } from '../lib/buefy/src/components/radio'
// import { Table, TableColumn } from '../lib/buefy/src/components/table'
import Tabs from '../lib/buefy/src/components/tabs/Tabs.vue'
import TabItem from '../lib/buefy/src/components/tabs/TabItem.vue'
//  Taglist
import Tag from '../lib/buefy/src/components/tag/Tag.vue'
import Autocomplete from '../lib/buefy/src/components/autocomplete/Autocomplete.vue'
// import Collapse from '../lib/buefy/src/components/collapse'
import Datepicker from '../lib/buefy/src/components/datepicker/Datepicker.vue'
// import Dialog from '../lib/buefy/src/components/dialog'
import Field from '../lib/buefy/src/components/field/Field.vue'
import Icon from '../lib/buefy/src/components/icon/Icon.vue'
import Input from '../lib/buefy/src/components/input/Input.vue'
// import Message from '../lib/buefy/src/components/message'
// import ModalProgrammatic, { Modal } from '../lib/buefy/src/components/modal'
// import Notification from '../lib/buefy/src/components/notification'
// import Pagination from '../lib/buefy/src/components/pagination'
// import Panel from '../lib/buefy/src/components/panel'
import Select from '../lib/buefy/src/components/select/Select.vue'
// import Snackbar from '../lib/buefy/src/components/snackbar'
import Switch from '../lib/buefy/src/components/switch/Switch.vue'
import Taginput from '../lib/buefy/src/components/taginput/Taginput.vue'
// import Timepicker from '../lib/buefy/src/components/timepicker'
// import Toast from '../lib/buefy/src/components/toast'
// import Tooltip from '../lib/buefy/src/components/tooltip'
import Upload from '../lib/buefy/src/components/upload/Upload.vue'

import config, { setOptions } from '../lib/buefy/src/utils/config'

setOptions(Object.assign(config, {
  defaultIconPack: 'fas',
  defaultDayNames: ['L', 'M', 'Mc', 'J', 'V', 'S', 'D'],
  defaultMonthNames: [
    'Enero',
    'Febrero',
    'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Ahosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
  ],
  defaultDateFormatter: (date) => {
    const f = new Intl.DateTimeFormat('es-MX').format(date)
    return f
  },
}))


window.Vue = Vue
library.add([
  faSearch,
  faPlus,
  faTags,
  faFileDownload,
  faExclamationCircle,
  faAngleLeft,
  faAngleRight,
])


function open(propsData) {
  const vm = Vue
  const DialogComponent = vm.extend(Dialog)
  return new DialogComponent({
    el: document.createElement('div'),
    propsData,
  })
}
const DialogProgrammatic = {
  alert(params) {
    let message
    if (typeof params === 'string') message = params
    const defaultParam = {
      canCancel: false,
      message,
    }
    const propsData = Object.assign(defaultParam, params)
    return open(propsData)
  },
  confirm(params) {
    const defaultParam = {}
    const propsData = Object.assign(defaultParam, params)
    return open(propsData)
  },
  prompt(params) {
    const defaultParam = {
      hasInput: true,
      confirmText: 'Done',
    }
    const propsData = Object.assign(defaultParam, params)
    return open(propsData)
  },
}

const LoadingProgrammatic = {
  open(params) {
    const defaultParam = {
      programmatic: true,
    }
    const propsData = Object.assign(defaultParam, params)

    const vm = Vue
    const LoadingComponent = vm.extend(Loading)
    return new LoadingComponent({
      el: document.createElement('div'),
      propsData,
    })
  },
}

Vue.component('FaIcon', FontAwesomeIcon)
Vue.component(Autocomplete.name, Autocomplete)
Vue.component(Checkbox.name, Checkbox)
// Vue.component(CheckboxButton.name, CheckboxButton)
// Vue.component(Collapse.name, Collapse)
Vue.component(Datepicker.name, Datepicker)
// Vue.component(Dropdown.name, Dropdown)
// Vue.component(DropdownItem.name, DropdownItem)
Vue.component(Field.name, Field)
Vue.component(Icon.name, Icon)
Vue.component(Input.name, Input)
// Vue.component(Loading.name, Loading)
// Vue.component(Message.name, Message)
// Vue.component(Modal.name, Modal)
// Vue.component(Notification.name, Notification)
// Vue.component(Pagination.name, Pagination)
// Vue.component(Panel.name, Panel)
// Vue.component(Radio.name, Radio)
// Vue.component(RadioButton.name, RadioButton)
Vue.component(Select.name, Select)
Vue.component(Switch.name, Switch)
Vue.component(TabItem.name, TabItem)
// Vue.component(Table.name, Table)
// Vue.component(TableColumn.name, TableColumn)
Vue.component(Tabs.name, Tabs)
Vue.component(Tag.name, Tag)
// Vue.component(Taglist.name, Taglist)
Vue.component(Taginput.name, Taginput)
// Vue.component(Timepicker.name, Timepicker)
// Vue.component(Tooltip.name, Tooltip)
Vue.component(Upload.name, Upload)
Vue.component('generic-form', GenericForm)

Vue.prototype.$confirmDeleteResource = (resourcePath) => {
  DialogProgrammatic.confirm({
    title: 'Borrando recurso',
    message: '¿Estas seguro de que quieres borrar este recurso?',
    confirmText: 'Borrar recurso',
    type: 'is-danger',
    hasIcon: true,
    onConfirm: () => {
      const loadingComponent = LoadingProgrammatic.open()

      axios.delete(resourcePath)
        .then(() => {
          loadingComponent.close()
          console.log('Eliminado')
          DialogProgrammatic.alert({
            message: 'Recurso eliminado correctamente',
            onConfirm: () => {
              window.location.reload()
            },
          })
        })
        .catch((error) => {
          loadingComponent.close()
          console.error(error)
          DialogProgrammatic.alert('Hubo un error borrando el recurso')
        })
    },
  })
}
