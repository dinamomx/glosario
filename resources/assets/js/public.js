import {
  library,
} from '@fortawesome/fontawesome-svg-core'
import {
  faSearch,
  faPrint,
  faDownload,
  faFileDownload,
  faPlay,
  faStop,
  faPause,
  faVolumeDown,
  faVolumeUp,
  faVolumeOff,
  faBars,
  faSadTear,
  faArrowRight,
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons'

import {
  faFacebook,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons'
import Vue from 'vue'
import {
  FontAwesomeIcon,
} from '@fortawesome/vue-fontawesome'
import VAudio from './components/VAudio.vue'
import VTracker from './components/VTracker.vue'

library.add([
  faSearch,
  faPrint,
  faDownload,
  faFileDownload,
  faFacebook,
  faTwitter,
  faPlay,
  faStop,
  faPause,
  faVolumeDown,
  faVolumeUp,
  faVolumeOff,
  faBars,
  faSadTear,
  faArrowRight,
  faArrowLeft,
])

window.Vue = Vue

Vue.component('FaIcon', FontAwesomeIcon)
Vue.component('v-audio', VAudio)
Vue.component('v-tracker', VTracker);

// Insertando fuente de google
(() => {
  const s = document.createElement('link')
  s.href = 'https://fonts.googleapis.com/css?family=Yeseva+One'
  s.rel = 'stylesheet'
  s.async = true
  const h = document.getElementsByTagName('link')[0]
  h.parentNode.insertBefore(s, h)
})()

class PrintPdf {
  constructor() {
    this.iframe = null
    this.url = ''
    this.failed = false
    /** @type {HTMLButtonElement} */
    this.button = null
    this.doPrint = () => {
      setTimeout(() => {
        try {
          this.iframe.contentWindow.print()
        } catch (error) {
          this.failed = true
          this.doModal()
        }
      }, 10)
    }
  }

  doModal() {
    let Modal = require('./ModalComponent.vue')
    Modal = Modal.default || Modal
    /** @type import('vue').VueConstructor */
    const ModalCtr = window.Vue.extend(Modal)
    const ModalDiv = document.createElement('div')
    document.body.appendChild(ModalDiv)
    this.modal = new ModalCtr({
      el: ModalDiv,
      propsData: {
        text: `<iframe src="${this.url}" style="width:100%;height:80vh;"></iframe>`,
      },
    })
    this.modal.$mount()
    // this.modal.$destroy()
  }

  /**
   * @description Callback para el click en botón "Imprimir"
   *
   * @param {HTMLButtonElement} button EL evento
   * @returns {boolean}
   *
   * @memberOf PrintPdf
   */
  print(button) {
    this.button = button
    console.log(this)
    this.url = this.button.dataset.target
    if (this.failed) {
      this.doModal()
      return
    }
    if (this.iframe) {
      this.doPrint()
      return
    }
    const iframe = document.createElement('iframe')
    this.iframe = iframe
    iframe.id = 'print-frame'
    iframe.classList.add('is-hidden')
    iframe.onload = this.doPrint
    document.body.appendChild(iframe)
    iframe.src = this.url
  }
}


window.$pdf = new PrintPdf()
