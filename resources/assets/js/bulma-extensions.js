import bulmaAccordion from 'bulma-extensions/bulma-accordion/dist/js/bulma-accordion'
import bulmaCalendar from 'bulma-extensions/bulma-calendar/dist/js/bulma-calendar'
import bulmaCarousel from 'bulma-extensions/bulma-carousel/dist/js/bulma-carousel'
import bulmaIconpicker from 'bulma-extensions/bulma-iconpicker/dist/js/bulma-iconpicker'
import bulmaQuickview from 'bulma-extensions/bulma-quickview/dist/js/bulma-quickview'
import bulmaSlider from 'bulma-extensions/bulma-slider/dist/js/bulma-slider'
import bulmaSteps from 'bulma-extensions/bulma-steps/dist/js/bulma-steps'
import bulmaTagsinput from 'bulma-extensions/bulma-tagsinput/dist/js/bulma-tagsinput'

// Instantiate
bulmaAccordion.attach()
bulmaCalendar.attach()
bulmaCarousel.attach()
bulmaIconpicker.attach()
bulmaQuickview.attach()
bulmaSlider.attach()
bulmaSteps.attach()
bulmaTagsinput.attach()
