if (typeof window.Vue !== 'function') {
  console.error('Vue no está en window')
} else {
  // window.Vue.config.performance = true
  const app = new window.Vue({
    el: '#__app',
    data() {
      return {
        laravel: window.laravel || {},
        demo: '',
      }
    },
  })

  window.$app = app
}
