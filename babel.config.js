module.exports = {
  presets: [
    ['@babel/preset-env', {
      modules: false,
      debug: true,
      useBuiltIns: 'entry',
      loose: true,
    }],
    '@vue/babel-preset-jsx',
  ],
  plugins: ['lodash',
    '@babel/transform-runtime',
    ['@babel/plugin-proposal-class-properties', { loose: true }],
  ],
}
